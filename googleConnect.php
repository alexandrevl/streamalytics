<?php
session_start();
session_destroy();
ini_set('session.gc_maxlifetime', 3600*10);
session_set_cookie_params(3600*10);
session_start();
header('Content-Type: text/html; charset=utf-8'); 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
unset($_SESSION['access_token']);
require_once '/home/cupul629/public_html/vendor/autoload.php';

$client = new Google_Client();
$client->setAuthConfig('/home/cupul629/client_secret.json');
$client->setAccessType("offline");        // offline access
$client->setIncludeGrantedScopes(true);   // incremental auth
$client->setScopes('https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/yt-analytics.readonly', 'https://www.googleapis.com/auth/yt-analytics-monetary.readonly');    
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/callback.php'); 

$auth_url = $client->createAuthUrl();
header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
?>