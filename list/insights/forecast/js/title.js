$(document).ready(function() {
    loadGames();
    var d = new Date();
    var weekday = new Array(7);
    weekday[6] = "5";
    weekday[0] = "6";
    weekday[1] = "0";
    weekday[2] = "1";
    weekday[3] = "2";
    weekday[4] = "3";
    weekday[5] = "4";
    var weekDayJs = d.getDay();
    console.log(weekDayJs);
    $('#weekdaySelect').val(weekday[weekDayJs]);
    $("#gameSelect").change(function() {
        forecast($(this).val());
    });
    $("#weekdaySelect").change(function() {
        if ($('#gameSelect').val() != 0) {
            forecast($('#gameSelect').val());
        }
    });
});

function loadGames() {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/video/stats/game/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    var i = 0;
    var gameTitleOld = ""
    json.forEach(function(game) {
        $('#gameSelect').append($('<option>', {
            value: game.gameId,
            text: game.gameTitle
        }));
    }, this);
}

function forecast(gameId) {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/insights/forecast/video/index.php?gameId=' + gameId,
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    forcastDuration = json.forecast;
    // forcastDuration.forEach(function(fDur) {
    //     fDur.forEach(function(forecast) {
    //         if (forecast.weekday == parseInt($('#weekdaySelect').val())) {
    //             $("#viewCount2hs").text((forecast.viewsMin * 120).toFixed(2));
    //             $("#avgViewers2hs").text(forecast.avgViewers.toFixed(2));
    //             $("#likeCount2hs").text((forecast.likesMin * 120).toFixed(2));

    //             $("#viewCount2hs").attr('class', 'alert alert-danger');
    //             if (forecast.viewsMin > parseFloat(json.avg.viewsMin)) {
    //                 $("#viewCount2hs").attr('class', 'alert alert-success');
    //             }
    //             $("#likeCount2hs").attr('class', 'alert alert-danger');
    //             if (forecast.likesMin > parseFloat(json.avg.likesMin)) {
    //                 $("#likeCount2hs").attr('class', 'alert alert-success');
    //             }
    //             $("#avgViewers2hs").attr('class', 'alert alert-danger');
    //             if (forecast.avgViewers > parseFloat(json.avg.avgViewers)) {
    //                 $("#avgViewers2hs").attr('class', 'alert alert-success');
    //             }
    //         }
    //     }, this);
    // }, this);
    forcastDuration[3].forEach(function(forecast) {
        if (forecast.weekday == parseInt($('#weekdaySelect').val())) {
            $("#viewCount2hs").text((forecast.viewsMin * 120).toFixed(2));
            $("#avgViewers2hs").text(forecast.avgViewers.toFixed(2));
            $("#likeCount2hs").text((forecast.likesMin * 120).toFixed(2));

            $("#viewCount2hs").attr('class', 'alert alert-danger');
            if (forecast.viewsMin > parseFloat(json.avg.viewsMin)) {
                $("#viewCount2hs").attr('class', 'alert alert-success');
            }
            $("#likeCount2hs").attr('class', 'alert alert-danger');
            if (forecast.likesMin > parseFloat(json.avg.likesMin)) {
                $("#likeCount2hs").attr('class', 'alert alert-success');
            }
            $("#avgViewers2hs").attr('class', 'alert alert-danger');
            if (forecast.avgViewers > parseFloat(json.avg.avgViewers)) {
                $("#avgViewers2hs").attr('class', 'alert alert-success');
            }
        }
    }, this);
    forcastDuration[5].forEach(function(forecast) {
        if (forecast.weekday == parseInt($('#weekdaySelect').val())) {
            $("#viewCount3hs").text((forecast.viewsMin * 180).toFixed(2));
            $("#avgViewers3hs").text(forecast.avgViewers.toFixed(2));
            $("#likeCount3hs").text((forecast.likesMin * 180).toFixed(2));

            $("#viewCount3hs").attr('class', 'alert alert-danger');
            if (forecast.viewsMin > parseFloat(json.avg.viewsMin)) {
                $("#viewCount3hs").attr('class', 'alert alert-success');
            }
            $("#likeCount3hs").attr('class', 'alert alert-danger');
            if (forecast.likesMin > parseFloat(json.avg.likesMin)) {
                $("#likeCount3hs").attr('class', 'alert alert-success');
            }
            $("#avgViewers3hs").attr('class', 'alert alert-danger');
            if (forecast.avgViewers > parseFloat(json.avg.avgViewers)) {
                $("#avgViewers3hs").attr('class', 'alert alert-success');
            }
        }
    }, this);
    //console.log(json);
}