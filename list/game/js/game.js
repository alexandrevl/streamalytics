$(document).ready(function() {
    //getGameTitle("Super Mario");
    $("#searchTitleGame").click(function() {
        var gameToSearch = $("#titleGame").val();
        getGameTitle(gameToSearch);
        //console.log(gameToSearch);
    });
    $('#titleGame').keyup(function(e) {
        if (e.keyCode == 13) {
            var gameToSearch = $("#titleGame").val();
            getGameTitle(gameToSearch);
        }
    });
});

function getGameTitle(gameTitleSearch) {
    $('#searchTitleGame').attr("disabled", true);
    $('#titleGame').attr("disabled", true);
    $("#tbody").empty();
    parameters = {};
    parameters.q = gameTitleSearch;
    $.ajax({
        type: 'POST',
        url: 'http://streamalytics.me/api/game/',
        data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData);
                //console.log(json);
                $('#divTableGames').show();
                json.forEach(function(game) {
                    $('#tableGames > tbody:last-child').append(
                        '<tr>' +
                        '<td>' + game.gameTitle + '</td>' +
                        '<td>' + game.namePlataform + '</td>' +
                        '</tr>');
                }, this);
                $('#searchTitleGame').attr("disabled", false);
                $('#titleGame').attr("disabled", false);
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {
        //console.log("error");
    });

}