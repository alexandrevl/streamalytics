<?php 
//session_id($session_id);
//error_reporting(E_ALL);
session_start(); 
//echo $_SESSION["auth"]["channelId"]; 
require_once $_SERVER['DOCUMENT_ROOT'] . '/forceAuth.php';  
?>   
<html>
    <head> 
        <title>
            Streamalytics - Dashboard
        </title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/dashboard.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110005302-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110005302-1');
        </script>

        <style>
            .cssHeaderRow {
                background-color: #2A94D6;
            }
            .cssTableRow {
                background-color: #F0F1F2;
                width: 100%;
            }
            .cssOddTableRow {
                background-color: #F0F1F2;
            }
            .cssSelectedTableRow {
                font-size: 12px;
                font-weight:bold;
            }
            .cssHoverTableRow {
                background: #ccc;
            }
            .cssHeaderCell {
                color: #FFFFFF;
                font-size: 12px;
                padding: 8px !important;
                border: solid 1px #FFFFFF;
                background-color: #2A94D6;
            }
            .cssTableCell {
                font-size: 12px;
                padding: 8px !important;
                border: solid 1px #FFFFFF;
                background-color: #F0F1F2;
                width: 100%;
            }
            .cssRowNumberCell {
                text-align: center;
            }
        </style>

    </head>
    <body>
            <div id="loading"><center><img src="../../img/loading_chart_small.gif"></center></div>
            <table class="table table-striped table-sm table-condensed" style='width: 100%;' id="listStats2">
                <thead id="thead"> 
                   
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
            <div id="table_div" style='width: 100%;' id="listStats"></div>
    </body>
</html>