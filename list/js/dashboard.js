$(document).ready(function() {
    google.charts.load('current', { 'packages': ['table'], 'language': 'pt-BR' });
    google.charts.setOnLoadCallback(drawTable);
    var parameters = {};
    parameters.order = "publishedAt desc";
    parameters.url = "http://streamalytics.me" + getUrlParameter('api');
    //updateTable(parameters);
});

var lastOrder = "";
var ascDesc = "desc";

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

function order(order) {
    var parameters = {};
    if (order == lastOrder) {
        if (ascDesc == "desc") {
            ascDesc = "asc"
        } else {
            ascDesc = "desc"
        }
    }
    lastOrder = order;
    parameters.order = order + " " + ascDesc;
    updateTable(parameters);
}

function drawTable() {
    var parameters = {};
    parameters.order = "publishedAt desc";
    parameters.url = "http://streamalytics.me" + getUrlParameter('api');
    console.log(parameters.url);
    var frozenColumns = 1;
    if (getUrlParameter('fc') > 0) {
        frozenColumns = getUrlParameter('fc');
    }
    $("#listStats").hide();
    $("#loading").show();
    $.ajax({
        type: 'POST',
        url: parameters.url,
        //data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                var data = new google.visualization.DataTable();


                var json = JSON.parse(resultData);
                //console.log(json);
                var i = 1;
                var fillHead = true;

                json.forEach(function(video) {
                    if (fillHead) {
                        for (title in video) {
                            if (title != 'channelId') {
                                if (isNumeric(video[title])) {
                                    data.addColumn('number', title);
                                } else {
                                    data.addColumn('string', title);
                                }
                            }
                        }
                        fillHead = false;
                    }
                }, this);
                //console.log(data);
                arrayAll = [];
                json.forEach(function(video) {
                    arrayData = [];
                    i = 0;
                    for (title in video) {
                        if (title != 'channelId') {
                            if (isNumeric(video[title])) {
                                //if (title === 'duration') {
                                arrayData[i++] = Number(video[title]);
                            } else {
                                arrayData[i++] = video[title];
                            }
                            if (video[title] == 20.5745) {
                                console.log(title);
                            }
                        }
                    }
                    //console.log(arrayData);
                    arrayAll.push(arrayData);
                }, this);
                var cssClassNames = {
                    'headerRow': 'cssHeaderRow',
                    'tableRow': 'cssTableRow',
                    'oddTableRow': 'cssOddTableRow',
                    'selectedTableRow': 'cssSelectedTableRow',
                    'hoverTableRow': 'cssHoverTableRow',
                    'headerCell': 'cssHeaderCell',
                    'tableCell': 'cssTableCell',
                    'rowNumberCell': 'cssRowNumberCell'
                };
                var options = {
                    showRowNumber: true,
                    width: 'auto',
                    height: 'auto',
                    frozenColumns: frozenColumns,
                    cssClassNames: cssClassNames
                };
                //console.log(arrayAll);
                data.addRows(arrayAll);

                var table = new google.visualization.Table(document.getElementById('table_div'));
                table.draw(data, options);
                console.log("Acabou 2");
                $("#listStats").show();
                $("#loading").hide();
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {});
}

function updateTable(parameters) {
    console.log(parameters.url);
    var fillHead = true;
    $("#listStats").hide();
    $("#loading").show();
    $("#tbody").empty();
    $.ajax({
        type: 'POST',
        url: parameters.url,
        data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                var json = JSON.parse(resultData);
                //console.log(json);
                var i = 1;

                json.forEach(function(video) {
                    if (fillHead) {
                        var head = '<tr>' +
                            '<th>#</th>';
                        for (title in video) {
                            //console.log(x);
                            if (title !== "channelId" && title !== "videoId") {
                                head += '<th>' + title + '</th>';
                            }
                        }
                        head += '</tr>';
                        $('#liststats > thead').replaceWith(head);
                        fillHead = false;
                    }
                    body = '<tr><th scope="row">' + i++ + '</th>';
                    for (title in video) {
                        if (title !== "channelId" && title !== "videoId") {
                            if (isFloat(video[title])) {
                                body += '<td>' + parseFloat(video[title]).toFixed(2) + '</td>';
                            } else {
                                body += '<td>' + video[title] + '</td>';
                            }
                        }
                    }
                    body += '</tr>';
                    $('#listStats > tbody:last-child').append(body);
                }, this);
                console.log("Acabou");
                $("#listStats").show();
                $("#loading").hide();
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {
        //console.log("error");
    });
}

function isFloat(x) { return !!(x % 1); }

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function typeVar() {

}