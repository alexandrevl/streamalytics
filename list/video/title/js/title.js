$(document).ready(function() {
    //getGameTitle("Super Mario");
    getVideos();

    // $("#openModal").click(function() {
    //     $('#divTableGames').hide();
    //     $("#titleGame").val("");
    //     $("#tbody").empty();
    //     console.log(videoId);
    // });

    $('#myModal').on('show.bs.modal', function(e) {
        $('#divTableGames').hide();
        $("#titleGame").val("");
        $("#tbody").empty();
        $("#videoId").val("");
        $("#update").val("");
        //console.log(e.relatedTarget) // do something...
        videoId = $(e.relatedTarget).data('videoid');
        update = $(e.relatedTarget).data('update');
        $("#videoId").val(videoId);
        $("#update").val(update);
    })


    $("#searchTitleGame").click(function() {
        var gameToSearch = $("#titleGame").val();
        getGameTitle(gameToSearch);
        //console.log(gameToSearch);
    });
    $('#titleGame').keyup(function(e) {
        if (e.keyCode == 13) {
            var gameToSearch = $("#titleGame").val();
            getGameTitle(gameToSearch);
        }
    });
});

function updateGame(plataformId, gameId, gameTitle, e) {
    // console.log($("#videoId").val());
    // console.log($("#update").val());
    // console.log(plataformId);
    // console.log(gameId);
    console.log(e.text);
    par = {};
    par.videoId = $("#videoId").val();
    par.update = $("#update").val();
    par.plataformId = plataformId;
    par.gameId = gameId;
    par.gameTitle = gameTitle;
    console.log(par);
    //console.log(JSON.stringify(par));

    $.ajax({
        type: 'POST',
        url: 'http://streamalytics.me/api/game/change/',
        data: JSON.stringify(par),
        //data: par,
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                console.log(resultData);
                //getVideos();
                e.text = par.gameTitle;
                console.log(unescape(par.gameTitle));
                idModal = "#openModal-" + par.videoId;
                $('#myModal').modal('hide');
                $(idModal).text(unescape(par.gameTitle));
                var jsonData = $.ajax({
                    url: 'http://streamalytics.me/setMainStats.php',
                    //dataType: "json",
                    async: false
                }).responseText;
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {
        //console.log("error");
    });
}

function getVideos() {
    $("#tbodyVideo").empty();
    $.ajax({
        type: 'POST',
        url: 'http://streamalytics.me/api/video/',
        //data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData);
                //console.log(json);
                $('#divTableVideos').show();
                json.forEach(function(video) {
                    var update = 0;
                    //console.log(video.gameTitle.length);
                    if (video.gameTitle.length == 0) {
                        video.gameTitle = null;
                    }
                    if (video.gameTitle != null) {
                        update = 1;
                    }
                    $('#tableVideos > tbody:last-child').append(
                        '<tr>' +
                        '<td><a href="https://www.youtube.com/watch?v=' + video.videoId + '" target="ytVideo">' + video.videoId + '</a></td>' +
                        '<td>' + video.title + '</td>' +
                        '<td>' + video.publishedAt + '</td>' +
                        '<td><a href="#" data-toggle="modal" data-target="#myModal" id="openModal-' + video.videoId + '" data-videoId="' + video.videoId + '" data-update="' + update + '">' + video.gameTitle + '</a></td>' +
                        '</tr>');
                }, this);
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {
        //console.log("error");
    });
}

function getGameTitle(gameTitleSearch) {
    $('#searchTitleGame').attr("disabled", true);
    $('#titleGame').attr("disabled", true);
    $("#tbody").empty();
    parameters = {};
    parameters.q = gameTitleSearch;
    $.ajax({
        type: 'POST',
        url: 'http://streamalytics.me/api/game/',
        data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData);
                //console.log(json);
                $('#divTableGames').show();
                json.forEach(function(game) {
                    $('#tableGames > tbody:last-child').append(
                        '<tr>' +
                        '<td><a href="#" onclick="updateGame(' + game.plataformId + ', ' + game.gameId + ', \'' + escape(game.gameTitle) + '\',this)">' + game.gameTitle + '</a></td>' +
                        '<td>' + game.namePlataform + '</td>' +
                        '</tr>');
                }, this);
                $('#searchTitleGame').attr("disabled", false);
                $('#titleGame').attr("disabled", false);
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {
        //console.log("error");
    });
}