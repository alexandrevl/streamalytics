<?php 
//session_id($session_id);
//error_reporting(E_ALL);
//session_start();  
//echo $_SESSION["auth"]["channelId"]; 
require_once $_SERVER['DOCUMENT_ROOT'] . '/forceAuth.php';  
?>   
<html>
    <head> 
        <title>
            Streamalytics - Dashboard  
        </title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/title.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110005302-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date()); 

        gtag('config', 'UA-110005302-1');
        </script>

        <style>
            .w-auto {
                width: auto;
            }
            .table-fixed {
                table-layout: fixed; 
                overflow: hidden;
            }
        </style>
    </head>
    <body>
        <br>
        <div id="divTableVideos" style="display: none;">
            <table class="table table-striped table-sm table-condensed" id="tableVideos">
                <thead>
                    <tr>
                        <th>VideoId</th>
                        <th>VideoTitle</th>
                        <th>PublishedAt</th>
                        <th>GameTitle</th>
                    </tr>
                </thead>
                <tbody id="tbodyVideo">
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change game</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"> 
                        <input type="text" class="form-control" id="videoId" placeholder="" maxlength="50" hidden>
                        <input type="text" class="form-control" id="update" placeholder="" maxlength="1" hidden>
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control" id="titleGame" placeholder="" maxlength="200">
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-primary" id="searchTitleGame">Search</button>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div id="divTableGames" style="display: none;">
                            <table class="table table-hover table-sm table-condensed" id="tableGames">
                                <thead>
                                    <tr>
                                        <th>Game Title</th>
                                        <th>Plataform</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>