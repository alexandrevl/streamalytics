$(document).ready(function() {
    //$("#main2").load('http://streamalytics.me/chart/video/weekday/ #target');
    // var a = $.ajax({
    //     url: 'http://streamalytics.me/list/index.php?api=/api/video/stats/game/ #target',
    //     //dataType: "json",
    //     async: false
    // }).responseText;
    // $("#main").innerHTML = a;
    //console.log(a);
    // $("#content").load(function() {
    //     resizeIframe(this);
    // });
    $("#videosList").click(function() {
        $("#content").attr("src", "/dashboard/");
    });
    $("#videosList2").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/&fc=2");
    });
    $("#videoPerWeek").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/week/");
    });
    $("#videoPerMonth").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/month/");
    });
    $("#videoPerPeriod").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/period/");
    });
    $("#videoPerWeekDay").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/weekday/");
    });
    $("#videoPerDuration").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/duration/");
    });
    $("#perGame").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/game/");
    });
    $("#gamePerMonth").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/game/month/");
    });
    $("#gamePerWeekday").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/video/stats/game/weekday/");
    });
    $("#changeGame").click(function() {
        $("#content").attr("src", "/list/video/title/");
    });
    $("#subsDate").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/subs/");
    });
    $("#forecastSubs").click(function() {
        $("#content").attr("src", "/list/index.php?api=/api/insights/forecast/subs/");
    });
    $("#chartSubs").click(function() {
        $("#content").attr("src", "/chart/subs/");
    });
    $("#chartGame").click(function() {
        $("#content").attr("src", "/chart/game/");
    });
    $("#chartVideoWeekday").click(function() {
        $("#content").attr("src", "/chart/video/weekday/");
    });
    $("#chartVideoMonthWeekday").click(function() {
        $("#content").attr("src", "/chart/video/month/weekday/");
    });
    $("#chartVideoMonthViewCount").click(function() {
        $("#content").attr("src", "/chart/video/month/");
    });
    $("#chartVideoWeekViewCount").click(function() {
        $("#content").attr("src", "/chart/video/week/");
    });
    $("#chartVideoWeekdayAvg").click(function() {
        $("#content").attr("src", "/chart/video/month/weekday/avgviewers/");
    });
    $("#forecastVideo").click(function() {
        $("#content").attr("src", "/list/insights/forecast/");
    });
    $("#chartForecastVideo").click(function() {
        $("#content").attr("src", "/chart/insights/forecast/video");
    });
});

function resizeIframe(obj) {
    obj.style.height = 0;
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    obj.style.width = 0;
    obj.style.width = obj.contentWindow.document.body.scrollWidth + 'px';
}