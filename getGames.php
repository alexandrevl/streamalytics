<?php
include 'Db.class.php';

session_start();
$db = new DB();
if (!isset($_SESSION["conn"])) {
    $_SESSION["conn"] = $db->connect();
}
$conn = $db->connect();

$url = 'http://thegamesdb.net/api/GetPlatformsList.php';

$plataforms = getGames($url);
echo 'Start...<br>';

$i = 0;
$sqlPlataform = "INSERT INTO game_plataform (plataformId, namePlataform, aliasPlataform) VALUES (?,?,?)";
$stmtPlataform = mysqli_prepare($conn, $sqlPlataform);
$sqlGames = "INSERT INTO game_details (plataformId, gameId, gameTitle, releaseDate) VALUES (?,?,?,?)";
$stmtGames = mysqli_prepare($conn, $sqlGames);

while ($plataforms['Platforms']['Platform'][$i]['id'] != null) { 
    //echo $plataforms['Platforms']['Platform'][$i]['id'] . "<br>";
    echo $plataforms['Platforms']['Platform'][$i]['name'] . "<br>";
    // echo $plataforms['Platforms']['Platform'][$i]['alias'] . "<br>";
    mysqli_stmt_bind_param($stmtPlataform, "iss", $plataforms['Platforms']['Platform'][$i]['id'], $plataforms['Platforms']['Platform'][$i]['name'], $plataforms['Platforms']['Platform'][$i]['alias']);
    mysqli_stmt_execute($stmtPlataform);
    //if ($plataforms['Platforms']['Platform'][$i]['id'] == 6) {
        $url = 'http://thegamesdb.net/api/GetPlatformGames.php?platform=' . $plataforms['Platforms']['Platform'][$i]['id'];
        $games = getGames($url);
        $j = 0;
        while ($games['Game'][$j] != null) {
            //print_r($games['Game'][$j]); 
            mysqli_stmt_bind_param($stmtGames, "iiss", $plataforms['Platforms']['Platform'][$i]['id'], $games['Game'][$j]['id'], $games['Game'][$j]['GameTitle'], $games['Game'][$j]['ReleaseDate']);
            mysqli_stmt_execute($stmtGames);
            ++$j;
        }
    //}
    ++$i; 
    //echo '<br><br>';
}
echo 'End';

function getGames($url) {
    //Once again, we use file_get_contents to GET the URL in question.
    $contents = file_get_contents($url);
    
    //If $contents is not a boolean FALSE value.
    if($contents !== false){
       //Print out the contents.
       //echo $contents;
    
    
       $xml = simplexml_load_string($contents);
       if ($xml === false) {
           echo "Failed loading XML: ";
           foreach(libxml_get_errors() as $error) {
               echo "<br>", $error->message;
           }
           return null;
       } else {
           $json = json_encode($xml);
           return json_decode($json,TRUE);
       }
    }
}
?>