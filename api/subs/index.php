<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php';

$data = file_get_contents('php://input');
$parameters = json_decode(file_get_contents('php://input'), true);

//print_r($parameters);


if (isset($channelId)) { 
    $db = new DB();
    if (!isset($_SESSION["conn"])) {
        $_SESSION["conn"] = $db->connect();
    }
    $conn = $db->connect();

    $games = array();

    $query = "SELECT
    date,
    subscribersGained,
    subscribersLost,
    total
    FROM
	yt_subs_date
    where channelId = '" . $channelId . "'";
    //$query = "select * from yt_mainStats where channelId = '" . $channelId . "'";

    if ($parameters['order'] != null) { 
        $query .= ' order by ' . $parameters['order'];
    } else {
        $query .= ' order by date';
    }

    if ($result = $conn->query($query)) {
        //print_r($result);
        while ($row = $result->fetch_assoc()) {
            $videos[] = $row;
        } 
        $json = json_encode($videos);

    } 
} else {
    $error = array( 
        'error' => 'ChannelId not found. Try autenticate again.'
    );
    $json = json_encode($error);
}
echo $json;


?>