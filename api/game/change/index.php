<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php';

$data = file_get_contents('php://input');
$parameters = json_decode(file_get_contents('php://input'), true);
 
// echo "chegou ". $data;
//print_r($parameters);
if (isset($channelId)) { 
    $db = new DB();
    $conn = $db->connect();

    if (intval($parameters['update']) == 1) {
        echo "Update";
        $sqlGame = "UPDATE yt_video_game set plataformId=?, gameId=?, gameTitle=? where videoId = ? and channelId = ?";
        $stmtGame = mysqli_prepare($conn, $sqlGame);
        $gameTitleDecode = urldecode($parameters['gameTitle']);
        mysqli_stmt_bind_param($stmtGame, "iisss", $parameters['plataformId'], $parameters['gameId'], $gameTitleDecode, $parameters['videoId'], $channelId);
        mysqli_stmt_execute($stmtGame);
        //mysqli_stmt_error ($stmtGame);
        $response = array(
            'return' => 'Update successfully'
        );
    } else {
        echo "Insert";
        $sqlGame = "INSERT INTO yt_video_game (channelId, videoId, plataformId, gameId, gameTitle) VALUES (?,?,?,?,?)";
        $stmtGame = mysqli_prepare($conn, $sqlGame);
        $gameTitleDecode = urldecode($parameters['gameTitle']);
        mysqli_stmt_bind_param($stmtGame, "ssiis", $channelId, $parameters['videoId'], $parameters['plataformId'], $parameters['gameId'], $gameTitleDecode);
        mysqli_stmt_execute($stmtGame);
        $response = array(
            'return' => 'Insert successfully'
        );
    }
    //setMainStats();

    $_SESSION["stats"] = null;
    
    $json = json_encode($response);
} else {
    $error = array( 
        'error' => 'ChannelId not found. Try autenticate again.'
    );
    $json = json_encode($error);
}
echo $json;



function setMainStats() {
    $db = new DB();
    $conn = $db->connect();

    echo "yt_mainStats start<br>";
    $query = "delete from yt_mainStats where channelId = '" . $channelId . "'";
    if ($result = $conn->query($query)) {
        echo $query . '<br>';
    }
    $stats = array();
    $query = "select * from mainStats where channelId = '" . $channelId . "'";
    if ($result = $conn->query($query)) {
        while ($row = $result->fetch_assoc()) {
            $stats[] = $row;
        } 
    }
    //print_r($stats);
    foreach ($stats as $video) {
        $columns = implode(", ",array_keys($video));
        $values = "";
        foreach ($video as $key => $value) {
            if (is_numeric ($value)) {
                $values .= $value . ",";
            } else {
                $values .= "'" . $value . "',";
            }
        }
        $values = rtrim($values,',');
        $sql = "INSERT INTO yt_mainStats ($columns) VALUES ($values)";
        echo $sql . "<br>";
        if ($result = $conn->query($sql)) {
            //echo "ok<br>";
        }
    }
    echo "yt_mainStats end<br>";
}

?>