<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php';

$data = file_get_contents('php://input');
$parameters = json_decode(file_get_contents('php://input'), true);

//print_r($parameters); 

$json = null;
if ($parameters != null) {
    $_SESSION["stats"]["video"] = null;
}
if (!isset($_SESSION["stats"]["video"])) {
    if (isset($channelId)) { 
        $db = new DB();
        if (!isset($_SESSION["conn"])) {
            $_SESSION["conn"] = $db->connect();
        }
        $conn = $db->connect();

        $games = array();

        $query = "select * from yt_mainStats where channelId = '" . $channelId . "' and isLive=1";

        if ($parameters['order'] != null) { 
            $query .= ' order by ' . $parameters['order'];
        } else {
            $query .= ' order by publishedAt desc';
        }

        if ($result = $conn->query($query)) {
            //print_r($result);
            while ($row = $result->fetch_assoc()) {
                $videos[] = $row;
            } 
            //print_r($videos);
            $json = json_encode($videos);
            $_SESSION["stats"]["video"] = $json;

        } 
    } else {
        $error = array( 
            'error' => 'ChannelId not found. Try autenticate again.'
        );
        $json = json_encode($error);
    }
} else {
    $json = $_SESSION["stats"]["video"];
}
echo $json;


?>