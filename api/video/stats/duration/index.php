<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php';

$data = file_get_contents('php://input');
$parameters = json_decode(file_get_contents('php://input'), true);

//print_r($parameters);

$json = null;
//$_SESSION["stats"]["duration"] = null;

if (!isset($_SESSION["stats"]["duration"])) {
    if (isset($channelId)) {  
        $db = new DB();
        if (!isset($_SESSION["conn"])) {
            $_SESSION["conn"] = $db->connect();
        }
        $conn = $db->connect();

        $games = array();

        $query = "select 
        CASE WHEN (duration/60) < 90  THEN '090-'
        WHEN (duration/60) BETWEEN 90 AND 120 THEN '090-120'
        WHEN (duration/60) BETWEEN 120 AND 150 THEN '120-150'
                WHEN (duration/60) BETWEEN 150 AND 180 THEN '150-180'
                WHEN (duration/60) BETWEEN 180 AND 210 THEN '180-210'
                WHEN (duration/60) BETWEEN 210 AND 240 THEN '210-240'
                ELSE '240+'
        END as minutesRange,
        count(videoId) as qnt,
        avg(duration) as duration, 
        avg(viewCount) as viewCount, 
        avg(likeCount) as likeCount,  
        avg(dislikeCount) as dislikeCount, 
        avg(commentCount) as commentCount, 
        avg(estimatedMinutesWatched) as estimatedMinutesWatched, 
        avg(comments) as comments,
        avg(shares) as shares,
        avg(averageViewDuration) as averageViewDuration,
        avg(averageViewPercentage) as averageViewPercentage,
        avg(subscribersGained) as subscribersGained,
        avg(viewsMin) as viewsMin,
        avg(likesMin) as likesMin,
        avg(commentsMin) as commentsMin,
        avg(subsMin) as subsMin,
        avg(minsView) as minsView,
        avg(commentsView) as commentsView,
        avg(viewsSubs) as viewsSubs,
        avg(likesSubs) as likesSubs,
        avg(engagement) as engagement,
        avg(avgViewers) as avgViewers
        from yt_mainStats
        where channelId = '" . $channelId . "' 
        and isLive = 1
        group by minutesRange
        order by minutesRange";
        //$query = "select * from yt_mainStats where channelId = '" . $channelId . "'";

        if ($parameters['order'] != null) { 
            $query .= ' order by ' . $parameters['order'];
        }

        if ($result = $conn->query($query)) {
            //print_r($result);
            while ($row = $result->fetch_assoc()) {
                $videos[] = $row;
            } 
            $json = json_encode($videos);

        } 
    } else {
        $error = array( 
            'error' => 'ChannelId not found. Try autenticate again.'
        );
        $json = json_encode($error);
    }
} else {
    $json = $_SESSION["stats"]["duration"];
}
$_SESSION["stats"]["duration"] = $json;
echo $json;


?>