<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php';

$data = file_get_contents('php://input');
$parameters = json_decode(file_get_contents('php://input'), true);

//print_r($parameters);

$json = null;
//$_SESSION["stats"]["weekday"] = null;
if (!isset($_SESSION["stats"]["weekdayMonth"])) {
    if (isset($channelId)) { 
        $db = new DB();
        if (!isset($_SESSION["conn"])) {
            $_SESSION["conn"] = $db->connect();
        }
        $conn = $db->connect();

        $games = array();

        $query = "select 
        month(publishedAt) as month, 
        weekday(publishedAt) as weekday, 
        count(videoId) as qnt,
        avg(duration) as duration, 
        avg(viewCount) as viewCount, 
        avg(likeCount) as likeCount,  
        avg(dislikeCount) as dislikeCount, 
        avg(commentCount) as commentCount, 
        avg(estimatedMinutesWatched) as estimatedMinutesWatched, 
        avg(comments) as comments,
        avg(shares) as shares,
        avg(averageViewDuration) as averageViewDuration,
        avg(averageViewPercentage) as averageViewPercentage,
        avg(subscribersGained) as subscribersGained,
        avg(viewsMin) as viewsMin,
        avg(likesMin) as likesMin,
        avg(commentsMin) as commentsMin,
        avg(subsMin) as subsMin,
        avg(minsView) as minsView,
        avg(commentsView) as commentsView,
        avg(viewsSubs) as viewsSubs,
        avg(likesSubs) as likesSubs,
        avg(engagement) as engagement,
        avg(avgViewers) as avgViewers
        from yt_mainStats
        where channelId = '" . $channelId . "'
        and isLive = 1
        group by weekday(publishedAt), month(publishedAt)
        order by month,weekday";
        //$query = "select * from yt_mainStats where channelId = '" . $channelId . "'";

        if ($parameters['order'] != null) { 
            $query .= ' order by ' . $parameters['order'];
        }

        if ($result = $conn->query($query)) {
            //print_r($result);
            while ($row = $result->fetch_assoc()) {
                $videos[] = $row;
            } 
            $json = json_encode($videos);
        } 
    } else {
        $error = array( 
            'error' => 'ChannelId not found. Try autenticate again.'
        );
        $json = json_encode($error);
    }
} else {
    $json = $_SESSION["stats"]["weekdayMonth"];
}
$_SESSION["stats"]["weekdayMonth"] = $json;
echo $json;


?>