<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php'; 

$data = file_get_contents('php://input'); 
$parameters = json_decode(file_get_contents('php://input'), true);

//print_r($parameters); 

$json = null;
$_SESSION["insights"]["video"] = null;
if ($parameters != null) { 
    $_SESSION["insights"]["video"] = null;
}
if (!isset($_SESSION["insights"]["video"])) {
    if (isset($channelId)) { 
        $db = new DB();
        if (!isset($_SESSION["conn"])) {
            $_SESSION["conn"] = $db->connect();
        }
        $conn = $db->connect();

        $gameId = 11958;
        if (isset($_GET['gameId'])) {
            $gameId = $_GET['gameId'];
        }


        $videos = array();

        $query = "
        select 
        *,
        week(publishedAt) as week,
        weekday(publishedAt) as weekday
        from yt_mainStats
        where isLive = 1
        and channelId = '" . $channelId . "' 
        and gameId = " . $gameId;

        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $videos[] = $row;
            } 
        } 

        $query = "select 
        week(publishedAt) as week, 
        avg(duration) as duration, 
        avg(viewCount) as viewCount, 
        avg(likeCount) as likeCount,  
        avg(dislikeCount) as dislikeCount, 
        avg(commentCount) as commentCount, 
        avg(estimatedMinutesWatched) as estimatedMinutesWatched, 
        avg(comments) as comments,
        avg(shares) as shares,
        avg(averageViewDuration) as averageViewDuration,
        avg(averageViewPercentage) as averageViewPercentage,
        avg(subscribersGained) as subscribersGained,
        avg(viewsMin) as viewsMin,
        avg(likesMin) as likesMin,
        avg(commentsMin) as commentsMin,
        avg(subsMin) as subsMin,
        avg(minsView) as minsView,
        avg(commentsView) as commentsView,
        avg(viewsSubs) as viewsSubs,
        avg(likesSubs) as likesSubs,
        avg(engagement) as engagement,
        avg(avgViewers) as avgViewers
        from yt_mainStats
        where channelId = '" . $channelId . "' 
        and isLive = 1
        group by week(publishedAt)";

        $weeks = array();
        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $weeks[] = $row;
            } 
        } 
        $query = "select 
        weekday(publishedAt) as weekday, 
        count(videoId) as qnt,
        avg(duration) as duration, 
        avg(viewCount) as viewCount, 
        avg(likeCount) as likeCount,  
        avg(dislikeCount) as dislikeCount, 
        avg(commentCount) as commentCount, 
        avg(estimatedMinutesWatched) as estimatedMinutesWatched, 
        avg(comments) as comments,
        avg(shares) as shares,
        avg(averageViewDuration) as averageViewDuration,
        avg(averageViewPercentage) as averageViewPercentage,
        avg(subscribersGained) as subscribersGained,
        avg(viewsMin) as viewsMin,
        avg(likesMin) as likesMin,
        avg(commentsMin) as commentsMin,
        avg(subsMin) as subsMin,
        avg(minsView) as minsView,
        avg(commentsView) as commentsView,
        avg(viewsSubs) as viewsSubs,
        avg(likesSubs) as likesSubs,
        avg(engagement) as engagement,
        avg(avgViewers) as avgViewers
        from yt_mainStats
        where channelId = '" . $channelId . "'
        and isLive = 1
        and publishedAt >= DATE_SUB(NOW(), INTERVAL 30 DAY)
        group by weekday(publishedAt)
        order by weekday";
        $weekDays = array();
        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $weekDays[] = $row;
            } 
        } 

        $query = "select 
        CASE 
        WHEN (duration/60) < 90  THEN '1'
        WHEN (duration/60) BETWEEN 90 AND 120 THEN '2'
        WHEN (duration/60) BETWEEN 120 AND 150 THEN '3'
                WHEN (duration/60) BETWEEN 150 AND 180 THEN '4'
                WHEN (duration/60) BETWEEN 180 AND 210 THEN '5'
                WHEN (duration/60) BETWEEN 210 AND 240 THEN '6'
                ELSE '7'
        END as minutesRange,
        count(videoId) as qnt,
        avg(duration) as duration, 
        avg(viewCount) as viewCount, 
        avg(likeCount) as likeCount,  
        avg(dislikeCount) as dislikeCount, 
        avg(commentCount) as commentCount, 
        avg(estimatedMinutesWatched) as estimatedMinutesWatched, 
        avg(comments) as comments,
        avg(shares) as shares,
        avg(averageViewDuration) as averageViewDuration,
        avg(averageViewPercentage) as averageViewPercentage,
        avg(subscribersGained) as subscribersGained,
        avg(viewsMin) as viewsMin,
        avg(likesMin) as likesMin,
        avg(commentsMin) as commentsMin,
        avg(subsMin) as subsMin,
        avg(minsView) as minsView,
        avg(commentsView) as commentsView,
        avg(viewsSubs) as viewsSubs,
        avg(likesSubs) as likesSubs,
        avg(engagement) as engagement,
        avg(avgViewers) as avgViewers
        from yt_mainStats
        where channelId = '" . $channelId . "'
        and isLive = 1
        and publishedAt >= DATE_SUB(NOW(), INTERVAL 60 DAY)
        group by minutesRange
        order by minutesRange";
        $minutesRange = array();
        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $minutesRange[] = $row;
            } 
        } 

        $query = "select 
        count(videoId) as qnt,
        avg(duration) as duration, 
        avg(viewCount) as viewCount, 
        avg(likeCount) as likeCount,  
        avg(dislikeCount) as dislikeCount, 
        avg(commentCount) as commentCount, 
        avg(estimatedMinutesWatched) as estimatedMinutesWatched, 
        avg(comments) as comments,
        avg(shares) as shares,
        avg(averageViewDuration) as averageViewDuration,
        avg(averageViewPercentage) as averageViewPercentage,
        avg(subscribersGained) as subscribersGained,
        avg(viewsMin) as viewsMin,
        avg(likesMin) as likesMin,
        avg(commentsMin) as commentsMin,
        avg(subsMin) as subsMin,
        avg(minsView) as minsView,
        avg(commentsView) as commentsView,
        avg(viewsSubs) as viewsSubs,
        avg(likesSubs) as likesSubs,
        avg(engagement) as engagement,
        avg(avgViewers) as avgViewers
        from yt_mainStats
        where channelId = '" . $channelId . "'
        and isLive = 1
        and publishedAt >= DATE_SUB(NOW(), INTERVAL 60 DAY)";
        $avg60days = array();
        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $avg60days[] = $row;
            } 
        } 
        

        //print_r($avg60days);

        //print_r($weekDays); 
        $percentVideo = array();
        $averages = array();
        $iVideos = 0;
        foreach ($videos as $keyVideo => $video) {
            ++$iVideos;
            foreach ($weeks as $avgWeek) {
                if ($avgWeek['week'] == $video['week']) {
                    foreach ($avgWeek as $key => $value) {
                        if ($key != "week" && $key != "weekday") {
                            $averages[$key] += $video[$key];
                            if ($avgWeek[$key] != 0) {
                                $percentVideo[$key] += $video[$key]/$avgWeek[$key];
                            } else {
                                $percentVideo[$key] += 1;
                            }
                        }
                    }
                }
            }
        }
        $forecast = array();
        foreach ($percentVideo as $key => $value) {
            $percentVideo[$key] = ($percentVideo[$key] / $iVideos);
        }
        //print_r($percentVideo);
        // echo "<br><br><br>";
        // print_r($averages);
        // echo "<br><br><br>";
        // print_r($weekDays);
        // echo "<br><br><br>";
        $avgWeekDay = array();
        $iWeekDays = 0;
        foreach ($weekDays as $key => $value) {
            //echo $key . "<br>";
            ++$iWeekDays;
            foreach ($value as $key2 => $value2) {
                $avgWeekDay[$key2] += $value2;
            }
        }
        //print_r($avgWeekDay);
        // echo "<br><br><br>";
        // print_r($minutesRange);
        // echo "<br><br><br>";
        foreach ($avgWeekDay as $key => $value) {
            $avgWeekDay[$key] = $avgWeekDay[$key]/$iWeekDays;
        }
        $minutesRangePercent = array();
        foreach ($minutesRange as $key => $value) {
            foreach ($value as $key2 => $value2) {
                //print_r($key2);
                //print_r($value2); 
                //echo "<br><br><br>"; 

                if ($avg60days[0][$key2] > 0) { 
                    $minutesRangePercent[$key][$key2] = $value2/$avg60days[0][$key2];
                } else {
                    $minutesRangePercent[$key][$key2] = 1;
                }
            }
        }
        // echo "<br><br><br>";
        //print_r($minutesRangePercent );
        $forecastDuration = array();
        foreach ($minutesRangePercent as $duration => $mrp) {
            foreach ($averages as $key => $value) {
                    foreach ($weekDays as $weekDay) {
                        $forecast[$weekDay['weekday']]["weekday"] = $weekDay['weekday'];
                        //$forecast[$weekDay['weekday']][$key] = (($averages[$key] / $iVideos) * ($percentVideo[$key] + $weekDay[$key]/$avgWeekDay[$key] + $mrp[$key])/4);
                        $forecast[$weekDay['weekday']][$key] = ($avg60days[0][$key] * ($percentVideo[$key]*2 + $weekDay[$key]/$avgWeekDay[$key] + $mrp[$key])/4);
                    }
            } 
            $forecastDuration[$duration] = $forecast;
        }
        // print_r($forecast);
        // echo "<br><br><br>";
        $returnJson['forecast'] = $forecastDuration;
        $returnJson["avg"] = $avg60days[0];
        $json = json_encode($returnJson);
        $_SESSION["insights"]["video"] = $json;
    } else {
        $error = array( 
            'error' => 'ChannelId not found. Try autenticate again.'
        );
        $json = json_encode($error);
    }
} else {
    $json = $_SESSION["insights"]["video"];
}
echo $json;


?>