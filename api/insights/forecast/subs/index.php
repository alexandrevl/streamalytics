<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/checkAuth.php';
include $_SERVER['DOCUMENT_ROOT'] . '/Db.class.php'; 

$data = file_get_contents('php://input'); 
$parameters = json_decode(file_get_contents('php://input'), true);

//print_r($parameters); 

$json = null;
$_SESSION["stats"]["insights"] = null;
if ($parameters != null) { 
    $_SESSION["stats"]["insights"] = null;
}
if (!isset($_SESSION["stats"]["insights"])) {
    if (isset($channelId)) { 
        $db = new DB();
        if (!isset($_SESSION["conn"])) {
            $_SESSION["conn"] = $db->connect();
        }
        $conn = $db->connect();

        $games = array();
        $date = new DateTime();
        $dateFormated = $date->format('Y-m-d');

        $avg = forecast($conn, $channelId, $dateFormated);

        $date->sub(new DateInterval('P20D'));
        $dateFormated = $date->format('Y-m-d');
        $variation = ($avg/forecast($conn, $channelId, $dateFormated));
        if ($variation < 1) {
            //$variation = 1;
        }

        // $date->sub(new DateInterval('P20D'));
        // $dateFormated = $date->format('Y-m-d');
        // echo forecast($conn, $channelId, $dateFormated);

        // $date->sub(new DateInterval('P20D'));
        // $dateFormated = $date->format('Y-m-d');
        // echo forecast($conn, $channelId, $dateFormated);
        

        // if ($result = $conn->query($query)) {
        //     while ($row = $result->fetch_assoc()) {
        //         $avg = ($row['subscribersGained']-$row['subscribersLost'])/$days;
        //         if ($row['qnt'] > $days) {
        //             $avg = ($row['subscribersGained']-$row['subscribersLost'])/$row['qnt'];
        //         }
        //     } 
        // } 
        $query = "select max(date) as lastDate, max(total) as total from yt_subs_date where channelId = '" . $channelId . "'";
        //$avg = -10;
        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $lastDate = $row['lastDate'] ;
                $total = $row['total'] ;
            } 
        } 
        $target = [];
        for ($i=1000;$i<=100000;$i+=1000) {
            array_push($target,$i);
        }
        $insights = array();
        foreach ($target as $value) {
            if ($total < $value) {
                $subsTarget = $value-$total;
                $subsForecast = $total;
                $i = 0;
                $date = new DateTime($lastDate);
                $date->setTimezone(new DateTimeZone('America/Sao_Paulo'));
                $avgForecast = $avg;
                $isValidDate = true;
                while ($subsForecast < $value) {
                    ++$i;
                    if ($i%20 == 0) {
                        $avgForecast = $avgForecast*$variation;
                    }
                    $subsForecast = $subsForecast + $avgForecast;
                    //echo $subsForecast . '<br>'; 
                    date_add($date, date_interval_create_from_date_string('1 days'));
                    $year = $date->format('Y');
                    if (intval($year) >= 2021) {
                        $isValidDate = false;
                        break;
                    }
                }
                //print_r($date);
                // echo '<br>';
                // echo $subsForecast . '<br>';
                // echo $avgForecast . '<br>';
                // echo $variation . '<br>';
                if ($isValidDate) {
                    $toDate = [
                        "subs" => $value,
                        "date" => $date->format('Y-m-d')
                    ];
                    array_push($insights, $toDate);
                }
            }
        }
        
        // foreach ($target as $value) {
        //     //echo $total . "<br>";
        //     if ($total < $value) {
        //         $subsTarget = $value-$total;
        //         $forecast = ceil($subsTarget/$avg);
        //         $date = new DateTime($lastDate);
        //         $date->setTimezone(new DateTimeZone('America/Sao_Paulo'));
        //         //print_r($date);
        //         //echo "<br>";
        //         //echo $forecast . "<br>";
        //         if ($forecast > 0) { 
        //             date_add($date, date_interval_create_from_date_string($forecast .' days'));
        //             $toDate = [
        //                 "subs" => $value,
        //                 "date" => $date->format('Y-m-d')
        //             ];
        //             array_push($insights, $toDate);
        //         }
        //     }
        // }
        $jsonArray = array(
            "forecast" => $insights,
            "avg" => $avg,
            "variation" => $variation
        );
        //$insights['avg'] = $avg;
        $json = json_encode($jsonArray);
        //$json['avg'] = $avg;
        $_SESSION["stats"]["insights"] = $json;
    } else {
        $error = array( 
            'error' => 'ChannelId not found. Try autenticate again.'
        );
        $json = json_encode($error);
    }
} else {
    $json = $_SESSION["stats"]["insights"];
}
echo $json;


function forecast($conn, $channelId, $date) {
    //print_r($date);
    $avg = 0;

    $days = 22;

    $query = "select subscribersGained, subscribersLost from yt_subs_date 
    where date >= DATE_SUB('" . $date .  "', INTERVAL " . ($days+2) . " DAY) and channelId = '" . $channelId . "'";

    $higher = -99999999999;
    $lower = 99999999999;

    if ($result = $conn->query($query)) {
        //print_r($result);
        while ($row = $result->fetch_assoc()) {
            $variation = $row['subscribersGained'] - $row['subscribersLost'];
            if ($variation > $higher) {
                $higher = $variation;
            }
            if ($variation < $lower) {
                $lower = $variation;
            }
        }
        $sum = 0;
        $qnt = 0;
        //echo 'Alow... ' . $lower;
        $result->data_seek(0);
        while ($row = $result->fetch_assoc()) {
            $variation = $row['subscribersGained'] - $row['subscribersLost'];
            if ($variation < $higher && $variation > $lower) {
                //echo $variation . '<br>'; 
                $sum += $variation;
                ++$qnt;
            }
        }
        $avg = $sum/$qnt;
    }
    //echo $avg;
    return $avg;
}

?>