$(document).ready(function() {
    loadGames();
    var d = new Date();
    var weekday = new Array(7);
    weekday[6] = "5";
    weekday[0] = "6";
    weekday[1] = "0";
    weekday[2] = "1";
    weekday[3] = "2";
    weekday[4] = "3";
    weekday[5] = "4";
    var weekDayJs = d.getDay();
    console.log(weekDayJs);
    $('#weekdaySelect').val(weekday[weekDayJs]);
    //google.charts.load('current', { 'packages': ['line'] });
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
    var gameId = 25698;
    $("#curve_chart").hide();
    $("#gameSelect").change(function() {
        dayWeek = parseInt($('#weekdaySelect').val());
        reDraw($(this).val());
    });
    $("#weekdaySelect").change(function() {
        if ($('#gameSelect').val() != 0) {
            dayWeek = parseInt($('#weekdaySelect').val());
            reDraw($('#gameSelect').val());
        }
    });
});

gameIdToSearch = 25698;
dayWeek = 0;

var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var month = new Array(12);
month[1] = "Jan";
month[2] = "Feb";
month[3] = "Mar";
month[4] = "Apr";
month[5] = "May";
month[6] = "Jun";
month[7] = "Jul";
month[8] = "Aug";
month[9] = "Sep";
month[10] = "Oct";
month[11] = "Nov";
month[12] = "Dec";

function reDraw(gameId) {
    console.log(gameId);
    $("#curve_chart").show();
    gameIdToSearch = gameId;
    drawChart();
}

function drawChart() {
    gameId = gameIdToSearch;
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/insights/forecast/video/index.php?gameId=' + gameId,
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    //console.log(json);
    var forecastDuration = json.forecast;
    var avg = json.avg;

    arrayData = [
        ['Duration', 'Views', 'Avg Views (last 60 days)', 'Likes', 'Avg Likes (last 60 days)', 'Avg Viewers Watching', 'Avg Viewers Watching (avg) (last 60 days)'],
    ];

    var dur = 90;
    forecastDuration.forEach(function(value) {
        cleanValue = value[dayWeek];
        //console.log(cleanValue);
        var durationText = "";
        switch (dur) {
            case 90:
                durationText = "< 1:30";
                break;
            case 120:
                durationText = "2:00";
                break;
            case 150:
                durationText = "2:30";
                break;
            case 180:
                durationText = "3:00";
                break;
            case 210:
                durationText = "3:30";
                break;
            case 240:
                durationText = "4:00";
                break;
            case 270:
                durationText = "> 4:30";
                break;

            default:
                break;
        }
        arrayData.push([durationText, parseFloat(cleanValue['viewsMin'] * dur), parseFloat(avg.viewsMin * dur), parseFloat(cleanValue['likesMin'] * dur), parseFloat(avg.likesMin * dur), parseFloat(cleanValue['avgViewers']), parseFloat(avg.avgViewers)]);
        dur += 30;
    }, this);

    //console.log(arrayData);


    var data = google.visualization.arrayToDataTable(arrayData);

    var options = {
        title: 'Forecast of Views, Likes and Avg Viewers Watching ',
        //vAxis: { title: 'Duration' },
        hAxis: { title: 'Duration in hours', minValue: 0 },
        seriesType: 'line',
        legend: { position: 'bottom' },
        //series: { 3: { type: 'line' } }
        series: {
            0: { targetAxisIndex: 0, axis: 'Views', color: 'green', lineWidth: 5 },
            1: { targetAxisIndex: 0, axis: 'Avg Views', color: 'green', lineWidth: 1, visibleInLegend: false, lineDashStyle: [4, 4] },
            2: { targetAxisIndex: 0, axis: 'Likes', color: 'red', lineWidth: 5 },
            3: { targetAxisIndex: 0, axis: 'Avg Likes', color: 'red', lineWidth: 1, visibleInLegend: false, lineDashStyle: [4, 4] },
            4: { targetAxisIndex: 1, axis: 'Avg Viewers Watching', type: 'bars', color: '#00D9F7' },
            5: { targetAxisIndex: 1, axis: 'Avg Viewers Watching (avg)', color: 'blue', lineWidth: 1, visibleInLegend: false, lineDashStyle: [4, 4] },
        },
        vAxes: [
            { title: 'Views or Likes' }, // Nothing specified for axis 0
            { title: 'Avg Viewers Watching', minValue: 0 }
        ]
    };

    var chart = new google.visualization.ComboChart(document.getElementById('curve_chart'));
    //chart.draw(data, google.charts.Line.convertOptions(options));
    chart.draw(data, options);

}

function loadGames() {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/video/stats/game/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    var i = 0;
    var gameTitleOld = ""
    json.forEach(function(game) {
        $('#gameSelect').append($('<option>', {
            value: game.gameId,
            text: game.gameTitle
        }));
    }, this);
}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}