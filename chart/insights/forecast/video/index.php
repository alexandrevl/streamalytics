<?php 
//session_id($session_id);
//error_reporting(E_ALL);
//session_start();  
//echo $_SESSION["auth"]["channelId"]; 
//require_once $_SERVER['DOCUMENT_ROOT'] . '/forceAuth.php';  
?>   
<html>
    <head> 
        <title>
            Streamalytics - Dashboard 
        </title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/chart.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110005302-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date()); 

        gtag('config', 'UA-110005302-1');
        </script>

        <style>
            .w-auto {
                width: auto;
            }
            .table-fixed {
                table-layout: fixed; 
                overflow: hidden;
            }
        </style>
    </head>
    <body> 
        <br>
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <select class="form-control" id="gameSelect">
                        <option>Select a game</option>
                    </select>
                </div>
                <div class="col-2">
                    <select class="form-control" id="weekdaySelect">
                        <option value="0">Monday</option>
                        <option value="1">Tuesday</option>
                        <option value="2">Wednesday</option>
                        <option value="3">Thursday</option>
                        <option value="4">Friday</option>
                        <option value="5">Saturday</option>
                        <option value="6">Sunday</option>
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <br>
                <div id="curve_chart" style="width: 90%; height: 90%;"></div>
            </div>
        </div>
    </body>
</html>