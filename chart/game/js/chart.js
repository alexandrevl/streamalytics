$(document).ready(function() {
    loadGames();
    google.charts.load('current', { 'packages': ['line'] });
    google.charts.setOnLoadCallback(drawChart);
});

var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var month = new Array(12);
month[1] = "Jan";
month[2] = "Feb";
month[3] = "Mar";
month[4] = "Apr";
month[5] = "May";
month[6] = "Jun";
month[7] = "Jul";
month[8] = "Aug";
month[9] = "Sep";
month[10] = "Oct";
month[11] = "Nov";
month[12] = "Dec";

function loadGames() {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/video/stats/game/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    var i = 0;
    var gameTitleOld = ""
    json.forEach(function(game) {
        $("#gameDropdown").append('<a class="dropdown-item" href="#" onClick="reDraw(' + game.gameId + ')">' + game.gameTitle + '</a>');
    }, this);
}

gameIdToSearch = 17011;

function reDraw(gameId) {
    gameIdToSearch = gameId;
    drawChart();
}

function drawChart() {
    var parameters = {};
    parameters.order = "publishedAt asc";
    var jsonData = $.ajax({
        type: 'POST',
        url: 'http://streamalytics.me/api/video/',
        data: JSON.stringify(parameters),
        //dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Date');
    data.addColumn('number', "ViewCount");
    data.addColumn('number', "AvgViewers");

    // arrayData = [
    //     ['Month', 'ViewCount'],
    // ];
    arrayData = [];
    json.forEach(function(value) {
        if (gameIdToSearch == value['gameId']) {
            $("#gameTitle").text(value.gameTitle);
            dt = value['publishedAt'].substring(0, 10);;
            arrayData.push([dt, parseFloat(value['viewCount']), parseFloat(value['avgViewers'])]);
        }
    }, this);

    console.log(arrayData);
    data.addRows(arrayData);
    //var data = google.visualization.arrayToDataTable(arrayData);

    var options = {
        chart: {
            title: 'Average ViewCount and Views/min'
        },
        width: 900,
        height: 500,
        series: {
            // Gives each series an axis name that matches the Y-axis below.
            0: { axis: 'ViewCount' },
            1: { axis: 'AvgViewers' }
        },
        axes: {
            // Adds labels to each axis; they don't have to match the axis names.
            y: {
                Temps: { label: 'Views' },
                Daylight: { label: 'AvgViewers' }
            }
        }
    };

    var chart = new google.charts.Line(document.getElementById('curve_chart'));
    chart.draw(data, google.charts.Line.convertOptions(options));

}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}