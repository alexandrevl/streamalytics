$(document).ready(function() {
    //google.charts.load('current', { 'packages': ['line'] });
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
});

var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var month = new Array(12);
month[1] = "Jan";
month[2] = "Feb";
month[3] = "Mar";
month[4] = "Apr";
month[5] = "May";
month[6] = "Jun";
month[7] = "Jul";
month[8] = "Aug";
month[9] = "Sep";
month[10] = "Oct";
month[11] = "Nov";
month[12] = "Dec";

function drawChart() {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/video/stats/month/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    // var data = new google.visualization.DataTable();
    // data.addColumn('string', 'Month');
    // data.addColumn('number', "ViewCount");
    // data.addColumn('number', "View/min");

    arrayData = [
        ['Month', 'Views', 'Views/Min', 'AvgViewers'],
    ];
    //arrayData = [];
    // json.forEach(function(value) {
    //     arrayData.push([month[value['month']], parseFloat(value['viewCount']), parseFloat(value['viewsMin'])]);
    // }, this);
    // console.log(arrayData);
    // data.addRows(arrayData);

    json.forEach(function(value) {
        arrayData.push([month[value['month']], parseFloat(value['viewCount']), parseFloat(value['viewsMin']), parseFloat(value['avgViewers'])]);
    }, this);

    // var data = google.visualization.arrayToDataTable([
    //     ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
    //     ['2004/05', 165, 938, 522, 998, 450, 614.6],
    //     ['2005/06', 135, 1120, 599, 1268, 288, 682],
    //     ['2006/07', 157, 1167, 587, 807, 397, 623],
    //     ['2007/08', 139, 1110, 615, 968, 215, 609.4],
    //     ['2008/09', 136, 691, 629, 1026, 366, 569.6]
    // ]);
    //console.log(arrayData);

    var data = google.visualization.arrayToDataTable(arrayData);

    //var data = google.visualization.arrayToDataTable(arrayData);

    var options = {
        title: 'Average ViewCount and Views/min',
        // vAxis: { title: 'Cups' },
        // hAxis: { title: 'Month' },
        seriesType: 'line',
        legend: { position: 'bottom' },
        //series: { 3: { type: 'line' } }
        series: {
            0: { targetAxisIndex: 0, axis: 'ViewCount', color: 'green', lineWidth: 5 },
            1: { targetAxisIndex: 1, axis: 'ViewsMin', color: 'red', lineWidth: 5 },
            2: { targetAxisIndex: 2, axis: 'AvgViewers', type: 'bars', color: 'orange' },
        },
        vAxes: {
            0: {
                title: 'Views'
            },
            2: {
                title: 'Avg Watching'
            }
        }
    };

    var chart = new google.visualization.ComboChart(document.getElementById('curve_chart'));
    //chart.draw(data, google.charts.Line.convertOptions(options));
    chart.draw(data, options);

}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}