$(document).ready(function() {
    google.charts.load("current", { packages: ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
});

var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var month = new Array(12);
month[1] = "Jan";
month[2] = "Feb";
month[3] = "Mar";
month[4] = "Apr";
month[5] = "May";
month[6] = "Jun";
month[7] = "Jul";
month[8] = "Aug";
month[9] = "Sep";
month[10] = "Oct";
month[11] = "Nov";
month[12] = "Dec";


function drawChart3() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Sales', 'Expenses', 'Profit'],
        ['2014', 1000, 400, 200],
        ['2015', 1170, 460, 250],
        ['2016', 660, 1120, 300],
        ['2017', 1030, 540, 350]
    ]);

    var options = {
        chart: {
            title: 'Company Performance',
            subtitle: 'Sales, Expenses, and Profit: 2014-2017',
        }
    };

    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
}

function drawChart() {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/video/stats/month/weekday/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);

    arrayData = [
        ['WeekDay', 'Sep', 'Oct', 'Nov'],
    ];

    arrayData[0][0] = 'WeekDay';
    i = 1;
    var actualMonth = (new Date().getMonth()) + 1;
    if (new Date().getDate() < 3) {
        actualMonth--;
        if (actualMonth == -1) {
            actualMonth = 12;
        }
    }
    for (let index = (actualMonth - 2); index <= actualMonth; index++) {
        arrayData[0][i++] = month[index];
    }
    console.log(arrayData);
    dayMonth = [
        [],
        [],
        [],
        [],
        [],
        [],
        []
    ];
    json.forEach(function(value) {
        //var actualMonth = (new Date().getMonth()) + 1;
        weekdaynum = parseInt(value['weekday']);
        weekdaymonth = parseInt(value['month']);
        if (weekdaymonth > actualMonth - 3) {
            dayMonth[weekdaynum][weekdaymonth] = parseFloat(value['viewCount']);
        }
    }, this);

    for (weekdayI in dayMonth) {
        arrayToPush = [];
        monthArray = dayMonth[weekdayI];
        arrayToPush[0] = weekday[weekdayI];
        arrayToPush[1] = null;
        arrayToPush[2] = null;
        arrayToPush[3] = null;
        i = 1;
        for (month in monthArray) {
            arrayToPush[i++] = monthArray[month];
        }
        arrayData.push(arrayToPush);
    }

    var data = google.visualization.arrayToDataTable(arrayData);

    var options = {
        chart: {
            title: 'ViewCount per weekday',
            subtitle: 'Views, weekday ast 3 months',
        }
    };
    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}