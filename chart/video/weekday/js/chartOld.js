$(document).ready(function() {
    google.charts.load("current", { packages: ['corechart'] });
    var parameters = {};
    parameters.order = "publishedAt desc";
    parameters.url = "http://streamalytics.me/api/video/stats/weekday/";
    updateChart(parameters);
});
var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var lastOrder = "";
var ascDesc = "desc";

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

function drawChart(arrayData) {
    console.log(arrayData);
    var data = google.visualization.arrayToDataTable(arrayData);

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
        {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
        },
        2
    ]);
    var options = {
        title: "View Count / Weekday",
        width: 800,
        height: 600,
        bar: { groupWidth: "95%" },
        legend: { position: "none" },
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
}

function updateChart(parameters) {
    console.log(parameters.url);
    var fillHead = true;
    $.ajax({
        type: 'POST',
        url: parameters.url,
        data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                var json = JSON.parse(resultData);
                //console.log(json);
                var i = 1;
                var arrayData = [
                    ["WeekDay", "ViewCount", { role: "style" }]
                ];
                json.forEach(function(value) {
                    arrayData.push([weekday[parseInt(value['weekday'])], parseFloat(value['viewCount']), makeRandomColor()]);
                }, this);
                // var arrayData = [
                //     ["Element", "Density", { role: "style" }],
                //     ["Copper", 8.94, "#b87333"],
                //     ["Silver", 10.49, "silver"],
                //     ["Gold", 19.30, "gold"],
                //     ["Platinum", 21.45, "color: #e5e4e2"]
                // ];
                google.charts.setOnLoadCallback(drawChart(arrayData));
                //drawChart(arrayData);
                console.log("Acabou");
            },
            404: function(resultData) {
                console.log("erro");
            },
            default: function(resultData) {
                console.log(resultData);
            }
        }
    }).fail(function() {
        //console.log("error");
    });
}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}