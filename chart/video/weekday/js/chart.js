$(document).ready(function() {
    google.charts.load("current", { packages: ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
});

var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var month = new Array(12);
month[1] = "Jan";
month[2] = "Feb";
month[3] = "Mar";
month[4] = "Apr";
month[5] = "May";
month[6] = "Jun";
month[7] = "Jul";
month[8] = "Aug";
month[9] = "Sep";
month[10] = "Oct";
month[11] = "Nov";
month[12] = "Dec";

function drawChart() {
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/video/stats/weekday/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);

    var arrayData = [
        ["WeekDay", "ViewCount", { role: "style" }]
    ];
    json.forEach(function(value) {
        arrayData.push([weekday[parseInt(value['weekday'])], parseFloat(value['viewCount']), 'blue']);
    }, this);
    var data = google.visualization.arrayToDataTable(arrayData);

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
        {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
        },
        2
    ]);
    var options = {
        title: "View Count / Weekday",
        width: 800,
        height: 600,
        bar: { groupWidth: "95%" },
        legend: { position: "none" },
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}