$(document).ready(function() {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
});

var weekday = new Array(7);
weekday[0] = "Mon";
weekday[1] = "Tue";
weekday[2] = "Wed";
weekday[3] = "Thu";
weekday[4] = "Fri";
weekday[5] = "Sat";
weekday[6] = "Sun";

var month = new Array(12);
month[1] = "Jan";
month[2] = "Feb";
month[3] = "Mar";
month[4] = "Apr";
month[5] = "May";
month[6] = "Jun";
month[7] = "Jul";
month[8] = "Aug";
month[9] = "Sep";
month[10] = "Oct";
month[11] = "Nov";
month[12] = "Dec";

function drawChart() {
    var param = {};
    param.order = "date asc";
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/subs/',
        dataType: "json",
        async: false
    }).responseText;
    var json = JSON.parse(jsonData);
    var jsonData = $.ajax({
        url: 'http://streamalytics.me/api/insights/forecast/subs/',
        dataType: "json",
        async: false
    }).responseText;
    var jsonForecast = JSON.parse(jsonData);

    arrayData = [
        ['Date', 'Total', 'Forecast'],
    ];
    //console.log(jsonForecast);
    var lastDate = "";
    var lastTotal = ""
    var i = 0;
    json.forEach(function(value) {
        ++i;
        d = new Date(Date.parse(value['date']));
        d.setTime(d.getTime() + d.getTimezoneOffset() * 60 * 1000);
        arrayData.push([d, parseInt(value['total']), null]);
        lastDate = value['date'];
        lastTotal = parseInt(value['total']);
    }, this);
    ///console.log(arrayData);
    //arrayData[i] = [lastDate, lastTotal, lastTotal];
    var i = 1;
    jsonForecast.forecast.forEach(function(value) {
        d = new Date(Date.parse(value['date']));
        d.setTime(d.getTime() + d.getTimezoneOffset() * 60 * 1000);
        if (i == 1) {
            ld = new Date(Date.parse(lastDate));
            ld.setTime(ld.getTime() + ld.getTimezoneOffset() * 60 * 1000);
            subsForecast = lastTotal;
            ld = addDays(ld, 1);
            var avgForecast = parseFloat(jsonForecast.avg);
            var varForecast = parseFloat(jsonForecast.variation);
            var daysForecast = 0;
            while (ld < d) {
                ++daysForecast;
                if (daysForecast % 20 == 0) {
                    avgForecast = avgForecast * varForecast;
                }
                //console.log(ld);
                subsForecast += avgForecast;
                arrayData.push([ld, null, Math.floor(subsForecast)]);
                ld = addDays(ld, 1);
            }
        }
        if (i++ <= 2) {
            arrayData.push([d, null, value['subs']]);
        }
    }, this);

    //console.log(arrayData);
    var data = google.visualization.arrayToDataTable(arrayData);

    var options = {
        title: 'Total of subscribers per day',
        height: 'auto',
        width: 'auto',
        legend: { position: 'bottom' }
    };

    var chart = new google.visualization.AreaChart(document.getElementById('curve_chart'));

    chart.draw(data, options);

}

function isFloat(x) { return !!(x % 1); }

function makeRandomColor() {
    var c = '';
    while (c.length < 7) {
        c += (Math.random()).toString(16).substr(-6).substr(-1)
    }
    return '#' + c;
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}