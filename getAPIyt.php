<?php
    session_start();
    error_reporting(E_ERROR | E_PARSE);
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    date_default_timezone_set('UTC-03:00');
    require_once '/home/cupul629/public_html/vendor/autoload.php';
    require_once 'checkAuth.php';
    include 'Db.class.php';
    $timeStart = time();
    echo "Updating videos from youtube<br>";
    echo "start...<br>";
    $db = new DB(); 
    if (!isset($_SESSION["conn"])) {
        $_SESSION["conn"] = $db->connect();
    }
    //unset($_SESSION["access_token"]);
    try { 
        $client = new Google_Client();
        $client->setAccessToken($_SESSION["access_token"]); 
    } catch (InvalidArgumentException $e) { 
        $client = null;
        echo "Erro google token. <br>";
    }
    $videoIdList= "";
    $conn = $db->connect();
    if (!isRunning($conn) && isGoogle($client)) {
        processRunning($conn,1);

        $timestamp = date("Y-m-d H:i:s");
        $duration = 0;
        $i = 0;
        $processVideoDetails = true;


        if (isset($_GET['reset'])) {
            if ($_GET['reset'] == 1) {
                $query = "delete from yt_video_details_json where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
                $query = "delete from yt_video_details where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
                $query = "delete from yt_video_analytics_json where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
                $query = "delete from yt_video_analytics where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
                $query = "delete from yt_subs_date where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
                $query = "delete from yt_video_stats where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
                $query = "delete from yt_mainStats where channelId = '" . $channelId . "'";
                if ($result = $conn->query($query)) {
                    echo $query . '<br>';
                }
            }
        } else {
            $query = "select max(lastUpdate) as lastUpdate from channel_process_log where channelId = '" . $channelId . "'";
            $lastUpdate7d = null;
            $lastUpdate = null;
            $date = "";
            if ($result = $conn->query($query)) {
                while ($row = $result->fetch_assoc()) {
                    if ($row['lastUpdate'] != null) {
                        $lastUpdate = $row['lastUpdate'];
                        $date = new DateTime($lastUpdate);
                        $lastUpdate = $date->format('Y-m-d');
                    }
                } 
            }
            $today = date("Y-m-d");
            if ($lastUpdate != null) {
                if ($lastUpdate < $today) {
                    $date = $date->sub(new DateInterval('P7D'));
                    $lastUpdate7d = $date->format('Y-m-d');
                    $videoIdList = "(";
                    //$query = "delete from yt_video_details where publishedAt > '" . $lastUpdate7d . "' and channelId = '" . $channelId . "'";
                    $query = "select videoId from yt_video_details where publishedAt > '" . $lastUpdate7d . "' and channelId = '" . $channelId . "'";
                    if ($result = $conn->query($query)) {
                        echo $query . '<br>';
                        while ($row = $result->fetch_assoc()) {
                            $videoIdList .= "'" . $row['videoId'] . "',";
                        }
                    }
                    $videoIdList = substr($videoIdList, 0, (strlen($videoIdList)-1)) . ")";
                    deleteVideos($conn, $channelId, $videoIdList);
                } else {
                    $processVideoDetails = false;
                }
            }
        }

        $yt_video_stats = array();
        if ($processVideoDetails) {
            echo "yt_video_details running...<br>";
            try {
                $optParamsStatic = array( 
                    'channelId' => $channelId,
                    'type'=> 'video', 
                    'order'=> 'date',
                    'maxResults' => 50,
                );
                $optParamsStatic = array( 
                    'forMine' => 'true',
                    'type'=> 'video', 
                    'order'=> 'date',
                    'maxResults' => 50,
                );
                $optParams = $optParamsStatic;
                $service = new Google_Service_YouTube($client);
                $nextPage = "1";
                $i = 0;

                while ($nextPage != null) { 
                    echo "page " . $i . "<br>";
                    $results = $service->search->listSearch("id", $optParams) ; 
                    //echo $conn;
                    $sqlJson = "INSERT INTO yt_video_details_json (channelId, videoId, json) VALUES (?,?,?)";
                    $stmtJson = mysqli_prepare($conn, $sqlJson);
                    $sqlVideo = "INSERT INTO yt_video_details (channelId, videoId, title, publishedAt, duration, viewCount, likeCount, dislikeCount, favoriteCount, commentCount, isLive) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                    $stmtVideo = mysqli_prepare($conn, $sqlVideo);
                    $nextPage = $results["nextPageToken"];
                    if ($nextPage != null) {
                        $optParams = $optParamsStatic;
                        $optParams["pageToken"] = $nextPage;
                    }
                    //echo $results["nextPageToken"] . "<br>";
                    //echo $results["prevPageToken"] . "<br>";
                    foreach ($results as $item) { 
                        $videoId = $item['id']['videoId']; 
                        $optParams2 = array(
                            'id'=> $videoId
                        );
                        $videoData = $service->videos->listVideos("snippet,contentDetails,fileDetails,statistics", $optParams2) ; 
                        foreach ($videoData as $videoItem) { 
                            $dataTemp = substr($videoItem['snippet']['publishedAt'],0,10);
                            $publishedAt = new DateTime($dataTemp);
                            $publishedAt = $publishedAt->format('Y-m-d');
                            if ($lastUpdate7d == null || $publishedAt >= $lastUpdate7d) {
                                $json = json_encode($videoData);
                                mysqli_stmt_bind_param($stmtJson, "sss", $channelId, $videoItem['id'], $json);
                                mysqli_stmt_execute($stmtJson);
                                $durationVideo = convertDuration($videoItem['contentDetails']['duration']);

                                $isLive = 0;
                                if ($videoItem['fileDetails']['fileName'] == "livestream.str") {
                                    $isLive = 1;
                                }
                                $dateRaw = new DateTime($videoItem['snippet']['publishedAt'], new DateTimeZone('Europe/London'));
                                $dateRaw->setTimezone(new DateTimeZone('America/Sao_Paulo'));
                                $dateRaw->sub(new DateInterval($videoItem['contentDetails']['duration']));

                                $yt_video_stats[$videoItem['id']] = array();
                                $yt_video_stats[$videoItem['id']]['duration'] = $durationVideo;
                                $yt_video_stats[$videoItem['id']]['title'] = $videoItem['snippet']['title'];
                                $yt_video_stats[$videoItem['id']]['views'] = $videoItem['statistics']['viewCount'];
                                $yt_video_stats[$videoItem['id']]['likes'] = $videoItem['statistics']['likeCount'];
                                $yt_video_stats[$videoItem['id']]['date'] = $dateRaw->format('Y-m-d');
                                //$dateRaw = new DateTime($videoItem['snippet']['publishedAt'], new DateTimeZone('Europe/London'));

                                mysqli_stmt_bind_param($stmtVideo, "ssssiiiiiii", $channelId, $videoItem['id'], $videoItem['snippet']['title'], $dateRaw->format('Y-m-d H:i:s'), $durationVideo, $videoItem['statistics']['viewCount'], $videoItem['statistics']['likeCount'],$videoItem['statistics']['dislikeCount'],$videoItem['statistics']['favoriteCount'],$videoItem['statistics']['commentCount'],$isLive);
                                mysqli_stmt_execute($stmtVideo);
                                //echo mysqli_stmt_error($stmtVideo) . "<br>";
                            } else {
                                $nextPage = null;
                            }
                        }
                    }
                    ++$i;
                    //$i = 10;
                    if ($i > 20) {
                        $nextPage = null;
                        echo "[Erro] Number of pages reached...<br>";
                    }
                }
                echo "yt_video_details done<br>";
            } catch (Google_Service_Exception $e) {
                $error = array( 
                    'error' => 'Google Error:' . $e->getMessage(),
                );
                $json = json_encode($error);
            }

            $daysAnalytics = 360;
            $totalSubs = 0;
            $subsArray = [];
            while ($daysAnalytics >= 90) {
                $end_date = date('Y-m-d', strtotime("-" . ($daysAnalytics-90) . " days"));
                $start_date = date('Y-m-d', strtotime("-" . $daysAnalytics . " days"));
                echo "Start DT: " . $start_date . " - End DT: " . $end_date . "<br>";
                processAnalytics($start_date, $end_date, $client, $conn, $yt_video_stats, $channelId);
                processSubs($start_date, $end_date, $client, $conn, $channelId, $totalSubs, $subsArray);
                $daysAnalytics -= 90;
            }
            
            //print_r($subsArray);
            echo "yt_video_stats running...<br>";

            $gamesArray = array();
            
            $query = "select plataformId, gameId, gameTitle from game_details where LENGTH(gameTitle) > 3 limit 50000";
            
            if ($result = $conn->query($query)) {
                while ($row = $result->fetch_assoc()) {
                    $gamesArray[] = $row;
                }  
            }

            $videoGamesArray = array();
            $query = "select videoId from yt_video_game where channelId='" . $channelId . "'";
            if ($result = $conn->query($query)) {
                while ($row = $result->fetch_assoc()) {
                    $videoGamesArray[] = $row;
                }  
            }
            
            $sqlStats = "INSERT INTO yt_video_stats (channelId, videoId, viewsMin, likesMin, commentsMin, subsMin, minsView, commentsView, viewsSubs, likesSubs, engagement, avgViewers) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            $stmtStats = mysqli_prepare($conn, $sqlStats);
            $sqlGame = "INSERT INTO yt_video_game (channelId, videoId, plataformId, gameId, gameTitle) VALUES (?,?,?,?,?)";
            $stmtGame = mysqli_prepare($conn, $sqlGame);
            $i = 1; 
            foreach ($yt_video_stats as $videoId => $value) {
                $matchGame = array(
                    "gameTitle" => "",
                    "plataformId" => 0,
                    "gameId" => 0
                );
                $searchGame = true;
                foreach ($videoGamesArray as $id => $videoIdGame) {
                    if ($videoIdGame['videoId'] == $videoId) {
                        $searchGame = false;
                    }    
                }
                if ($searchGame) {
                    foreach ($gamesArray as $keyGame => $gameTitle) {
                        if (strpos(strtolower($value['title']), strtolower($gameTitle['gameTitle'])) !== false) {
                            if (strlen($gameTitle['gameTitle']) > strlen($matchGame['gameTitle'])) {
                                $matchGame['gameTitle'] = $gameTitle['gameTitle'];
                                $matchGame['plataformId'] = $gameTitle['plataformId'];
                                $matchGame['gameId'] = $gameTitle['gameId'];
                            }
                        }
                    }
                } 
                
                if ($value['viewsMin'] != null) {
                    //$engagement = (($value['viewsMin']*$value['averageViewPercentage']*$value['averageViewDuration'])/$subsArray[$value['date']]); 
                    $avgViewers =  ($value['averageViewDuration']/$value['duration'])*$value['views'];
                    $engagement = 10*($value['commentsMin']*$avgViewers*($value['likes']/$value['views']))/$subsArray[$value['date']]; 
                    ++$i;
                    try {
                        mysqli_stmt_bind_param($stmtStats, "ssdddddddddd", $channelId, $videoId, $value['viewsMin'], $value['likesMin'], $value['commentsMin'], $value['subsMin'], $value['minsView'], $value['commentsView'], $value['viewsSubs'], $value['likesSubs'], $engagement, $avgViewers);
                        mysqli_stmt_execute($stmtStats);
                        if ($matchGame['plataformId'] != 0) {
                            mysqli_stmt_bind_param($stmtGame, "ssiis", $channelId, $videoId, $matchGame['plataformId'], $matchGame['gameId'], $matchGame['gameTitle']);
                            mysqli_stmt_execute($stmtGame);
                        }
                    } catch (Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }
                }
            }

            echo "yt_video_stats done<br>";

            echo "yt_mainStats start<br>";
            $query = "delete from yt_mainStats where channelId = '" . $channelId . "'";
            if ($result = $conn->query($query)) {
                //echo $query . '<br>';
            }
            $stats = array();
            $query = "select * from mainStats where channelId = '" . $_SESSION["auth"]["channelId"] . "'";
            $query = "CALL mainStatsFunction('" . $channelId . "');"; 
            if ($result = $conn->query($query)) {
                while ($row = $result->fetch_assoc()) {
                    $stats[] = $row;
                } 
                mysqli_free_result($result);
                mysqli_next_result($conn);
            }
            //print_r($stats);
            foreach ($stats as $video) {
                $columns = implode(", ",array_keys($video));
                $values = "";
                foreach ($video as $key => $value) {
                    if (is_numeric ($value)) {
                        $values .= $value . ",";
                    } else {
                        $values .= "'" . str_replace("'", "", $value) . "',";
                    }
                }
                $values = rtrim($values,',');
                $sql = "INSERT INTO yt_mainStats ($columns) VALUES ($values);";
                //echo $sql . "<br>";
                if ($result = $conn->query($sql)) {
                    // print_r($result);
                    // echo "<br>";
                } else {
                    printf("Error: %s<br>", $conn->error);
                }
            }
            echo "yt_mainStats end<br>";

            $isProcessing = 0;
            $duration = (time()-$timeStart);
            processLog($conn,$duration,$i);
            $_SESSION["stats"] = null;
            echo $i . " videos processed. Time: " . $duration . "s<br>";
        }
        processRunning($conn,0);
    } else {
        if (!isGoogle($client)) {
            echo "Google credentials invalid<br>";
        } else {
            echo "Process for this channel is running...<br>";
        }
    }
    echo "end";


    function convertDuration($duration) {
        $seconds = 0;
        $interval = new DateInterval($duration);
        $seconds += intval($interval->d) * 24*60*60;
        $seconds += intval($interval->h) * 60*60;
        $seconds += intval($interval->i) * 60;
        $seconds += intval($interval->s); 
        return $seconds;
    }
    function processRunning($conn,$isProcessing) {
        $sqlControl = "UPDATE channel_process_control set isProcessing=? where channelId=?";
        $stmtControl = mysqli_prepare($conn, $sqlControl);
        mysqli_stmt_bind_param($stmtControl, "is", $isProcessing, $_SESSION["auth"]["channelId"]);
        mysqli_stmt_execute($stmtControl);
    }
    function processLog($conn,$duration, $qntVideos) {
        $timestamp = date("Y-m-d H:i:s");
        $sqlControl = "INSERT INTO channel_process_log (channelId, lastUpdate, duration, qntVideos) VALUES (?,?,?,?)";
        $stmtControl = mysqli_prepare($conn, $sqlControl);
        mysqli_stmt_bind_param($stmtControl, "ssii", $_SESSION["auth"]["channelId"], $timestamp, $duration, $qntVideos);
        mysqli_stmt_execute($stmtControl);
    }
    function isRunning($conn) {
        $isRunning = false;
        $query = "select * from channel_process_control where channelId = '" . $_SESSION["auth"]["channelId"] . "' and isProcessing = 1";
        if ($result = $conn->query($query)) {
            while ($row = $result->fetch_assoc()) {
                $isRunning = true;
            } 
        }
        return $isRunning;
    }
    function isGoogle($client) {
        $isGoogle = true;
        if ($client != null) {
            try {
                $service = new Google_Service_YouTube($client);
            } catch (Google_Service_Exception $e) { 
                $isGoogle = false;
            }
        } else {
            $isGoogle = false;
        }
        return $isGoogle;
    }

    function processAnalytics($start_date, $end_date, &$client, &$conn, &$yt_video_stats, &$channelId) {
        //ANALYTICS!!!!!
        echo "yt_video_analytics running...<br>"; 
        //echo "Start DT: " . $start_date . " - End DT: " . $end_date . "<br>";
        $analytics = new Google_Service_YouTubeAnalytics($client);
        $ids = 'channel==MINE';
        // $end_date = date("Y-m-d"); 
        // $start_date = date('Y-m-d', strtotime("-90 days"));
        $optparams = array(
            'dimensions' => 'video',
            'max-results' => '200',
            'sort' => '-views',
            );

        $metric = 'views,likes,dislikes,estimatedMinutesWatched,comments,shares,averageViewDuration,averageViewPercentage,subscribersGained,subscribersLost';

        try{
            $api = $analytics->reports->query($ids, $start_date, $end_date, $metric, $optparams);
            //print_r($api);

            $sqlJson = "INSERT INTO yt_video_analytics_json (channelId, videoId, json) VALUES (?,?,?)";
            $stmtJson = mysqli_prepare($conn, $sqlJson);
            $sqlVideo = "INSERT INTO yt_video_analytics (channelId, videoId, views, likes, dislikes, estimatedMinutesWatched, comments, shares, averageViewDuration, averageViewPercentage, subscribersGained, subscribersLost) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            $stmtVideo = mysqli_prepare($conn, $sqlVideo);

            foreach ($api->rows as $r) {
                $videoId = $r[0];
                $json = json_encode($r);
                //echo $json . "<br>";
                mysqli_stmt_bind_param($stmtJson, "sss", $channelId, $videoId, $json);
                mysqli_stmt_execute($stmtJson);
                $views = $r[1];
                $likes = $r[2];
                $dislikes = $r[3];
                $estimatedMinutesWatched = $r[4];
                $comments = $r[5];
                $shares = $r[6];
                $averageViewDuration = $r[7];
                $averageViewPercentage = $r[8];
                $subscribersGained = $r[9];
                $subscribersLost = $r[10];

                if ($yt_video_stats[$videoId]['duration'] < floor(($averageViewDuration*100/$averageViewPercentage)-($yt_video_stats[$videoId]['duration']*0.1))) {
                    //echo $videoId . " Erro duration (" . $yt_video_stats[$videoId]['title'] . ") . De " . $yt_video_stats[$videoId]['duration'] . " - Para " . floor(($averageViewDuration*100/$averageViewPercentage)) . "<br>";
                    $yt_video_stats[$videoId]['duration'] = floor(($averageViewDuration*100/$averageViewPercentage));
                }

                $yt_video_stats[$videoId]['commentsMin'] = $comments/($yt_video_stats[$videoId]['duration']/60);
                $yt_video_stats[$videoId]['subsMin'] = $subscribersGained/($yt_video_stats[$videoId]['duration']/60);
                $yt_video_stats[$videoId]['minsView'] = $estimatedMinutesWatched/$yt_video_stats[$videoId]['views'];
                $yt_video_stats[$videoId]['commentsView'] = $comments/$yt_video_stats[$videoId]['views'];
                $yt_video_stats[$videoId]['viewsSubs'] = $yt_video_stats[$videoId]['views']/$subscribersGained;
                $yt_video_stats[$videoId]['likesSubs'] = $likes/$subscribersGained;
                $yt_video_stats[$videoId]['averageViewPercentage'] = $averageViewPercentage;
                $yt_video_stats[$videoId]['averageViewDuration'] = $averageViewDuration;
                //echo $videoId . ' - ' . $yt_video_stats[$videoId]['views'] . ' - ' . $yt_video_stats[$videoId]['duration'] . ' - ' . ($yt_video_stats[$videoId]['duration']/60) . '<br>'; 
                $yt_video_stats[$videoId]['viewsMin'] = $yt_video_stats[$videoId]['views']/($yt_video_stats[$videoId]['duration']/60);
                $yt_video_stats[$videoId]['likesMin'] = $yt_video_stats[$videoId]['likes']/($yt_video_stats[$videoId]['duration']/60);

                mysqli_stmt_bind_param($stmtVideo, "ssiiiiiiddii", $channelId, $videoId, $views, $likes, $dislikes, $estimatedMinutesWatched, $comments, $shares, $averageViewDuration, $averageViewPercentage, $subscribersGained, $subscribersLost);
                mysqli_stmt_execute($stmtVideo);
                //echo mysqli_stmt_error ($stmtVideo) . '<br>';
            }
            echo "yt_video_analytics done<br>";
        }catch (Google_Service_Exception $e) { 
            echo sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));

            header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . '/index.php', FILTER_SANITIZE_URL));
        }
    }

    function processSubs($start_date, $end_date, &$client, &$conn, &$channelId, &$totalSubs, &$subsArray) {
        echo "yt_subs_date running...<br>";
        //echo "Start DT: " . $start_date . " - End DT: " . $end_date . "<br>";
        $analytics = new Google_Service_YouTubeAnalytics($client);
        $ids = 'channel==MINE';
        $optparams = array(
            'dimensions' => 'day',
            'max-results' => '200',
            );

        $metric = 'subscribersGained,subscribersLost';
        try{
            $api = $analytics->reports->query($ids, $start_date, $end_date, $metric, $optparams);
            //print_r($api);

            $sqlSubs = "INSERT INTO yt_subs_date (channelId, date, subscribersGained, subscribersLost,total) VALUES (?,?,?,?,?)";
            $stmtSubs = mysqli_prepare($conn, $sqlSubs);
            
            foreach ($api->rows as $r) {
                $date = $r[0];
                $subscribersGained = $r[1];
                $subscribersLost = $r[2];
                $totalSubs += ($subscribersGained-$subscribersLost);
                $subsArray[$date] = $totalSubs;
                mysqli_stmt_bind_param($stmtSubs, "ssiii", $channelId, $date, $subscribersGained,$subscribersLost,$totalSubs);
                mysqli_stmt_execute($stmtSubs);
            }
            echo "yt_subs_date done<br>";
        }catch (Google_Service_Exception $e) { 
            echo sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
            header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . '/index.php', FILTER_SANITIZE_URL));
        }
    }





    function deleteVideos($conn, $channelId, $videoIdList) {
        $query = "delete from yt_video_details_json where channelId = '" . $channelId . "' and videoId in " . $videoIdList;
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            //print_r($result);
            echo "delete yt_video_details_json<br>";
        }
        $query = "delete from yt_video_details where channelId = '" . $channelId . "' and videoId in " . $videoIdList;
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            echo "delete yt_video_details<br>";
        }
        $query = "delete from yt_video_analytics_json where channelId = '" . $channelId . "' and videoId in " . $videoIdList;
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            echo "delete yt_video_analytics_json<br>";
        }
        $query = "delete from yt_video_analytics where channelId = '" . $channelId . "' and videoId in " . $videoIdList;
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            echo "delete yt_video_analytics<br>";
        }
        $query = "delete from yt_subs_date where channelId = '" . $channelId . "'";
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            echo "delete yt_subs_date<br>";
        }
        $query = "delete from yt_video_stats where channelId = '" . $channelId . "' and videoId in " . $videoIdList;
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            echo "delete yt_video_stats<br>";
        }
        $query = "delete from yt_mainStats where channelId = '" . $channelId . "' and videoId in " . $videoIdList;
        if ($result = $conn->query($query)) {
            //echo $query . '<br>';
            echo "delete yt_mainStats<br>";
        }

    }
?>