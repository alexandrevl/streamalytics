<?php 
session_start(); 
error_reporting(E_ERROR);
require_once $_SERVER['DOCUMENT_ROOT'] . '/forceAuth.php';  
?>   
<html>
    <head> 
        <title>
            Streamalytics - Dashboard
        </title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110005302-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110005302-1');
        </script>

        <style>
            .w-auto {
                width: auto;
            }
            .table-fixed {
                table-layout: fixed; 
                overflow: hidden;
            }
        </style>
    </head>
    <body>
        <a href="/list/index.php?api=/api/video/" target="week">Videos 2</a><br>
        <br>
        <a href="/list/video/title/" target="game">Change game in video</a><br>
        <br>
        <a href="/list/index.php?api=/api/video/stats/game/" target="week">Per Game</a><br>
        <a href="/list/index.php?api=/api/video/stats/game/month/" target="week">Game per Month</a><br>
        <a href="/list/index.php?api=/api/video/stats/game/weekday/" target="week">Game per WeekDay</a><br>
        <br>
        <a href="/list/index.php?api=/api/subs/" target="week">Subs</a><br>
        <a href="/chart/subs/" target="week">Chart Subs</a><br>
        <br>
        <a href="/chart/game/" target="week">Chart Game</a><br>
        <a href="/chart/video/weekday/" target="week">Chart Video Weekday</a><br>
        <a href="/chart/video/month/weekday" target="week">Chart Video Month Weekday</a><br>
        <a href="/chart/video/month/" target="week">Chart Video Month ViewCount</a><br>
        <a href="/chart/video/week/" target="week">Chart Video Week ViewCount</a><br>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-2"> 
                    <nav class="nav flex-column nav-pills">
                        <div class="btn-group-vertical" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" id="videosList2">Videos2</button>
                            <button type="button" class="btn btn-secondary" id="videosList">Videos</button>
                            <button type="button" class="btn btn-secondary" id="videoPerWeek">Video/Week</button>
                        </div>
                        <a class="nav-link" href="#" id="videoPerWeek">Video per Week</a>
                        <a class="nav-link" href="#" id="videoPerMonth">Video per Month</a>
                        <a class="nav-link" href="#" id="videoPerPeriod">Video per Period</a>
                        <a class="nav-link" href="#" id="videoPerWeekDay">Video per WeekDay</a>
                        <a class="nav-link" href="#" id="videoPerDuration">Video per Duration</a>
                    </nav>
                </div>
                <div class="col-10">
                    <iframe id="content" src="/list/index.php?api=/api/video/stats/game/" width="100%" height="100%" style="border:none;" scrolling="no"></iframe>
                </div>
            </div>
        </div>
    </body>
</html>