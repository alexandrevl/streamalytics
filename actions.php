<?php 
session_start(); 
error_reporting(E_ERROR);
require_once $_SERVER['DOCUMENT_ROOT'] . '/forceAuth.php';  
?>   
<html>
    <head> 
        <title>
            Streamalytics - Dashboard
        </title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110005302-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110005302-1');
        </script>

        <style>
            .w-auto {
                width: auto;
            }
            .table-fixed {
                table-layout: fixed; 
                overflow: hidden;
            }
        </style> 
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                        <div style="font-weight: bold;">Lists</div>
                        <div class="btn-group-vertical mr-2" role="group" aria-label="Basic example">
                            <button type="radio" class="btn btn-secondary" id="videosList2">Videos2</button>
                            <button type="radio" class="btn btn-secondary" id="videoPerWeek">Video/Week</button>
                            <button type="radio" class="btn btn-secondary" id="videoPerMonth">Video/Month</button>
                            <button type="radio" class="btn btn-secondary" id="videoPerPeriod">Video/Period</button>
                            <button type="radio" class="btn btn-secondary" id="videoPerWeekDay">Video/WeekDay</button>
                            <button type="radio" class="btn btn-secondary" id="videoPerDuration">Video/Duration</button>
                        </div>
                        <br><br>
                        <div class="btn-group-vertical mr-2" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" id="perGame">Per Game</button>
                            <button type="button" class="btn btn-secondary" id="gamePerMonth">Game/Month</button>
                            <button type="button" class="btn btn-secondary" id="gamePerWeekday">Game/WeekDay</button>
                            <button type="button" class="btn btn-secondary" id="changeGame">Change Game</button>
                        </div>
                        <br><br>
                        <div class="btn-group-vertical mr-2" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" id="subsDate">Subs/Date</button>
                            <button type="button" class="btn btn-secondary" id="chartSubs">Chart Subs</button>
                        </div>
                        <br><br>
                        <div style="font-weight: bold;">Forecast</div>
                        <div class="btn-group-vertical mr-2" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" id="forecastVideo">Video</button>
                            <button type="button" class="btn btn-secondary" id="chartForecastVideo">Chart Video</button>
                        </div>
                        <br><br>
                        <div style="font-weight: bold;">Charts</div>
                        <div class="btn-group-vertical mr-2" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary" id="chartGame">Games</button>
                            <button type="button" class="btn btn-secondary" id="chartVideoWeekday">Video/Weekday</button>
                            <button type="button" class="btn btn-secondary" id="chartVideoMonthWeekday">Month/Weekday</button>
                            <button type="button" class="btn btn-secondary" id="chartVideoMonthViewCount">Month/ViewCount</button>
                            <button type="button" class="btn btn-secondary" id="chartVideoWeekViewCount">Week/ViewCount</button>
                            <button type="button" class="btn btn-secondary" id="chartVideoWeekdayAvg">Weekday/AvgViewers</button>
                        </div>
                </div>
                <div class="col-10">
                    <iframe id="content" src="/getAPIyt.php" width="100%" height="100%" style="border:none;" seamless="seamless" scrolling="yes" frameBorder="0" marginwidth="0" marginheight="0"></iframe>
                </div>
            </div>
        </div>
    </body>
</html>