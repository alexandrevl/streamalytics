<?php
session_start();
include 'Db.class.php';

$db = new DB();
if (!isset($_SESSION["conn"])) {
    $_SESSION["conn"] = $db->connect();
}
$conn = $db->connect();

$games = array();

$query = "select gameTitle from game_details where LENGTH(gameTitle) > 3 limit 50000";

if ($result = $conn->query($query)) {
    //print_r($result);
    while ($row = $result->fetch_assoc()) {
        $games[] = $row;
    }  
    //print_r($games);
}

$videos = array();

$query = "select title from yt_video_details";

if ($result = $conn->query($query)) {
    //print_r($result);
    while ($row = $result->fetch_assoc()) {
        $videos[] = $row;
    } 
    //print_r($videoTitles);
}

foreach ($videos as $keyVideo => $videoTitle) {
    $matchGame = "";
    $x = 0;
    $percentMatched = 0;
    $wordsMatched = 0;
    $percent = 0;
    foreach ($games as $keyGame => $gameTitle) {
        // similar_text(strtolower($gameTitle['gameTitle']),strtolower($videoTitle['title']),$percent);

        // $gameTitleExploded = explode(" ",$gameTitle['gameTitle']);
        // foreach ($gameTitleExploded as $key => $value) {
        //     if ($value != null) {
        //         if (strpos(strtolower($videoTitle['title']), strtolower($value)) !== false) {
        //             ++$wordsMatched;
        //         }
        //     }
        //     if (($wordsMatched/sizeof($gameTitleExploded)) > 0.5) {
        //         if (strlen($gameTitle['gameTitle']) > strlen($matchGame)) {
        //             $matchGame = $gameTitle['gameTitle'];
        //         }
        //     }
        // }
        // $percent = compareStrings(strtolower($gameTitle['gameTitle']), strtolower($videoTitle['title']));
        // if ($percent > $percentMatched) {
        //     $percentMatched = $percent;
        //     $x = $gameTitle['gameTitle'];
        // }

        
        if (strpos(strtolower($videoTitle['title']), strtolower($gameTitle['gameTitle'])) !== false) {
            if (strlen($gameTitle['gameTitle']) > strlen($matchGame)) {
                $matchGame = $gameTitle['gameTitle'];
                //$x = levenshtein(strtolower($gameTitle['gameTitle']),strtolower($videoTitle['title']))/strlen($matchGame);
                $x = compareStrings(strtolower($gameTitle['gameTitle']), strtolower($videoTitle['title']));
            }
        }

    }
    echo $videoTitle['title'] . " - " . $matchGame . ' - ' . $percent . ' - ' . $x . '<br>';
    
    //print_r($videoTitle['title']);
}

function compareStrings($s1, $s2) {
    //one is empty, so no result
    if (strlen($s1)==0 || strlen($s2)==0) {
        return 0;
    }

    //replace none alphanumeric charactors
    //i left - in case its used to combine words
    $s1clean = preg_replace("/[^A-Za-z0-9-]/", ' ', $s1);
    $s2clean = preg_replace("/[^A-Za-z0-9-]/", ' ', $s2);

    //remove double spaces
    while (strpos($s1clean, "  ")!==false) {
        $s1clean = str_replace("  ", " ", $s1clean);
    }
    while (strpos($s2clean, "  ")!==false) {
        $s2clean = str_replace("  ", " ", $s2clean);
    }

    //create arrays
    $ar1 = explode(" ",$s1clean);
    $ar2 = explode(" ",$s2clean);
    $l1 = count($ar1);
    $l2 = count($ar2);

    //flip the arrays if needed so ar1 is always largest.
    if ($l2>$l1) {
        $t = $ar2;
        $ar2 = $ar1;
        $ar1 = $t;
    }

    //flip array 2, to make the words the keys
    $ar2 = array_flip($ar2);


    $maxwords = max($l1, $l2);
    $matches = 0;

    //find matching words
    foreach($ar1 as $word) {
        if (array_key_exists($word, $ar2))
            $matches++;
    }

    return ($matches / $maxwords) * 100;    
}

?>