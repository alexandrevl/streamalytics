<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    require_once '/home/cupul629/public_html/vendor/autoload.php';
    include 'Db.class.php';

    $db = new DB();
    if (!isset($_SESSION["conn"])) {
        $_SESSION["conn"] = $db->connect();
    }
    $conn = $db->connect();


    $videoCount = 0;
    $client = new Google_Client();
    $client->setAuthConfig('/home/cupul629/client_secret.json');
    $client->setAccessType("offline");        // offline access
    $client->setIncludeGrantedScopes(true);   // incremental auth
    //$client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setScopes('https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/yt-analytics.readonly', 'https://www.googleapis.com/auth/yt-analytics-monetary.readonly');    
    //$client->addScope(Google_Service_YouTube::YOUTUBE_READONLY);
    $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/Streamalytics/callback.php'); 

    if (!isset($_SESSION["access_token"])) {
        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $access_token = $client->getAccessToken();
            $_SESSION["access_token"] = $access_token;
        } else {
            header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . '/Streamalytics/index.php', FILTER_SANITIZE_URL));
        }
    }
    $client->setAccessToken($_SESSION["access_token"]);



    $yt_video_stats = array();
    //Youtube!!!
    $channelId = "";
    $service = new Google_Service_YouTube($client);
    $optParams = array('mine' => 'true');
    $results = $service->channels->listChannels("statistics,snippet,topicDetails,contentDetails", $optParams) ; 
    foreach ($results as $item) {
        $channelId = $item['id'];
        echo $item['id'], "<br /> \n";
        //echo '<img src="'. $item['snippet']['thumbnails']['default']['url'] . '"/> <br />';
    }
 
    try {
        $optParamsStatic = array(
            'channelId' => $channelId,
            'type'=> 'video', 
            'order'=> 'date',
            'maxResults' => 50,
        );
        $optParams = $optParamsStatic;
        $service = new Google_Service_YouTube($client);
        $nextPage = "1";
        $i = 0;

        while ($nextPage != null) { 
            $results = $service->search->listSearch("id", $optParams) ; 
            //echo $conn;
            $sqlJson = "INSERT INTO yt_video_details_json (channelId, videoId, json) VALUES (?,?,?)";
            $stmtJson = mysqli_prepare($conn, $sqlJson);
            $sqlVideo = "INSERT INTO yt_video_details (channelId, videoId, title, publishedAt, duration, viewCount, likeCount, dislikeCount, favoriteCount, commentCount, isLive) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            $stmtVideo = mysqli_prepare($conn, $sqlVideo);
            $nextPage = $results["nextPageToken"];
            if ($nextPage != null) {
                $optParams = $optParamsStatic;
                $optParams["pageToken"] = $nextPage;
            }
            echo $results["nextPageToken"] . "<br>";
            echo $results["prevPageToken"] . "<br>";
            foreach ($results as $item) { 
                $videoId = $item['id']['videoId']; 
                $optParams2 = array(
                    'id'=> $videoId
                );
                $videoData = $service->videos->listVideos("snippet,contentDetails,fileDetails,statistics", $optParams2) ; 
                foreach ($videoData as $videoItem) { 
                    $json = json_encode($videoData);
                    mysqli_stmt_bind_param($stmtJson, "sss", $channelId, $videoItem['id'], $json);
                    mysqli_stmt_execute($stmtJson);
                    $duration = convertDuration($videoItem['contentDetails']['duration']);

                    $isLive = 0;
                    if ($videoItem['fileDetails']['fileName'] == "livestream.str") {
                        $isLive = 1;
                    }
                    $yt_video_stats[$videoItem['id']] = array();
                    $yt_video_stats[$videoItem['id']]['duration'] = $duration;
                    $yt_video_stats[$videoItem['id']]['views'] = $videoItem['statistics']['viewCount'];
                    $yt_video_stats[$videoItem['id']]['viewsMin'] = $videoItem['statistics']['viewCount']/($duration/60);
                    $yt_video_stats[$videoItem['id']]['likesMin'] = $videoItem['statistics']['likeCount']/($duration/60);
                    mysqli_stmt_bind_param($stmtVideo, "ssssiiiiiii", $channelId, $videoItem['id'], $videoItem['snippet']['title'], $videoItem['snippet']['publishedAt'], $duration, $videoItem['statistics']['viewCount'], $videoItem['statistics']['likeCount'],$videoItem['statistics']['dislikeCount'],$videoItem['statistics']['favoriteCount'],$videoItem['statistics']['commentCount'],$isLive);
                    mysqli_stmt_execute($stmtVideo);
                }
            }
            ++$i;
            //$i = 10;
            if ($i > 5) {
                $nextPage = null;
                echo "Saiu por aqui..<br>";
            }
        }
        // print_r($yt_video_stats);
        // echo '<br>';
    } catch (Google_Service_Exception $e) {
        echo sprintf('<p>A service error occurred: <code>%s</code></p>',
        htmlspecialchars($e->getMessage()));
    }




    //ANALYTICS!!!!!
    $analytics = new Google_Service_YouTubeAnalytics($client);
    $ids = 'channel==MINE';
    $end_date = date("Y-m-d"); 
    $start_date = date('Y-m-d', strtotime("-180 days"));
    // $optparams = array(
    // 'dimensions' => 'day',
    // );

    $optparams = array(
        'dimensions' => 'video',
        'max-results' => '200',
        'sort' => '-views',
        );
    
    $metric = 'views,likes,dislikes,estimatedMinutesWatched,comments,shares,averageViewDuration,averageViewPercentage,subscribersGained,subscribersLost';
    
    try{

        $api = $analytics->reports->query($ids, $start_date, $end_date, $metric, $optparams);

        $sqlJson = "INSERT INTO yt_video_analytics_json (channelId, videoId, json) VALUES (?,?,?)";
        $stmtJson = mysqli_prepare($conn, $sqlJson);
        $sqlVideo = "INSERT INTO yt_video_analytics (channelId, videoId, views, likes, dislikes, estimatedMinutesWatched, comments, shares, averageViewDuration, averageViewPercentage, subscribersGained, subscribersLost) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmtVideo = mysqli_prepare($conn, $sqlVideo);

        foreach ($api->rows as $r) {
            $videoId = $r[0];
            $json = json_encode($r);
            echo $json . "<br>";
            mysqli_stmt_bind_param($stmtJson, "sss", $channelId, $videoId, $json);
            mysqli_stmt_execute($stmtJson);
            $views = $r[1];
            $likes = $r[2];
            $dislikes = $r[3];
            $estimatedMinutesWatched = $r[4];
            $comments = $r[5];
            $shares = $r[6];
            $averageViewDuration = $r[7];
            $averageViewPercentage = $r[8];
            $subscribersGained = $r[9];
            $subscribersLost = $r[10];

            $yt_video_stats[$videoId]['commentsMin'] = $comments/($yt_video_stats[$videoId]['duration']/60);
            $yt_video_stats[$videoId]['subsMin'] = $subscribersGained/($yt_video_stats[$videoId]['duration']/60);
            $yt_video_stats[$videoId]['minsView'] = $estimatedMinutesWatched/$yt_video_stats[$videoId]['views'];
            $yt_video_stats[$videoId]['commentsView'] = $comments/$yt_video_stats[$videoId]['views'];
            $yt_video_stats[$videoId]['viewsSubs'] = $yt_video_stats[$videoId]['views']/$subscribersGained;
            $yt_video_stats[$videoId]['likesSubs'] = $likes/$subscribersGained;

            // echo $videoId . "<br>";
            // echo $views . "<br>";
            // echo $estimatedMinutesWatched . "<br>";
            // echo $comments . "<br>";
            // echo $averageViewDuration . "<br>";
            // echo $averageViewPercentage . "<br>";
            // echo '----------------------------------<br>';
            mysqli_stmt_bind_param($stmtVideo, "ssiiiiiiddii", $channelId, $videoId, $views, $likes, $dislikes, $estimatedMinutesWatched, $comments, $shares, $averageViewDuration, $averageViewPercentage, $subscribersGained, $subscribersLost);
            mysqli_stmt_execute($stmtVideo);
        }
    }catch (Google_Service_Exception $e) { 
        echo sprintf('<p>A service error occurred: <code>%s</code></p>',
        htmlspecialchars($e->getMessage()));

        header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . '/Streamalytics/index.php', FILTER_SANITIZE_URL));
    }
    //echo "Chegou: " . $access_token;
    //print_r($yt_video_stats);

    $sqlStats = "INSERT INTO yt_video_stats (channelId, videoId, viewsMin, likesMin, commentsMin, subsMin, minsView, commentsView, viewsSubs, likesSubs, engagement) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    $stmtStats = mysqli_prepare($conn, $sqlStats);
    foreach ($yt_video_stats as $videoId => $value) {
        $engagement = 0.0;
        if ($value['viewsMin'] != null) {
            mysqli_stmt_bind_param($stmtStats, "ssddddddddd", $channelId, $videoId, $value['viewsMin'], $value['likesMin'], $value['commentsMin'], $value['subsMin'], $value['minsView'], $value['commentsView'], $value['viewsSubs'], $value['likesSubs'], $engagement);
            mysqli_stmt_execute($stmtStats);
        }
    }



    function convertDuration($duration) {
        $seconds = 0;
        $interval = new DateInterval($duration);
        $seconds += intval($interval->d) * 24*60*60;
        $seconds += intval($interval->h) * 60*60;
        $seconds += intval($interval->i) * 60;
        $seconds += intval($interval->s); 
        return $seconds;
    }
?>