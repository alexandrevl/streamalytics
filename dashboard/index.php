<?php 
//session_start(); 
//require_once $_SERVER['DOCUMENT_ROOT'] . '/forceAuth.php';  
?>   
<html>
    <head> 
        <title>
            Streamalytics - Dashboard
        </title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/dashboard.js" type="text/javascript"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110005302-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110005302-1');
        </script>

        <style>
            .w-auto {
                width: auto;
            }
            .table-fixed {
                table-layout: fixed; 
                overflow: hidden;
            }
        </style>
    </head>
    <body>
        <br>
            <div id="loading"><center><img src="../img/loading_chart_small.gif"></center></div>
            <table class="table table-striped table-sm table-condensed" style='width: 2600; table-layout: fixed;' id="listVideos">
                <thead>
                    <tr>
                    <th style='width: 2%'>#</th>
                    <th style='width: 28%'><a href="#" id="order" onclick="order('title')"><i class="fa fa-sort" aria-hidden="true"></i> Title</a></th>
                    <th style='width: 7%'><a href="#" id="order" onclick="order('game')"><i class="fa fa-sort" aria-hidden="true"></i> Game</a></th>
                    <th style='width: 7%'><a href="#" id="order" onclick="order('publishedAt')"><i class="fa fa-sort" aria-hidden="true"></i> Date</a></th>
                    <th style='width: 3%'><a href="#" id="order" onclick="order('duration')"><i class="fa fa-sort" aria-hidden="true"></i> Mins</a></th>
                    <th style='width: 3%'><a href="#" id="order" onclick="order('viewCount')"><i class="fa fa-sort" aria-hidden="true"></i> Views</a></th>
                    <th style='width: 3%'><a href="#" id="order" onclick="order('likeCount')"><i class="fa fa-sort" aria-hidden="true"></i> Likes</a></th>
                    <th style='width: 4%'><a href="#" id="order" onclick="order('dislikeCount')"><i class="fa fa-sort" aria-hidden="true"></i> Dislikes</a></th>
                    <th style='width: 4%'><a href="#" id="order" onclick="order('estimatedMinutesWatched')"><i class="fa fa-sort" aria-hidden="true"></i> MinWatch</a></th>
                    <th style='width: 5%'><a href="#" id="order" onclick="order('comments')"><i class="fa fa-sort" aria-hidden="true"></i> Comments</a></th>
                    <th style='width: 5%'><a href="#" id="order" onclick="order('averageViewDuration')"><i class="fa fa-sort" aria-hidden="true"></i> AvgView</a></th>
                    <th><a href="#" id="order" onclick="order('averageViewPercentage')"><i class="fa fa-sort" aria-hidden="true"></i> View %</a></th>
                    <th><a href="#" id="order" onclick="order('subscribersGained')"><i class="fa fa-sort" aria-hidden="true"></i> Subs</a></th>
                    <th><a href="#" id="order" onclick="order('viewsMin')"><i class="fa fa-sort" aria-hidden="true"></i> Views/min</a></th>
                    <th><a href="#" id="order" onclick="order('likesMin')"><i class="fa fa-sort" aria-hidden="true"></i> Likes/min</a></th>
                    <th><a href="#" id="order" onclick="order('minsView')"><i class="fa fa-sort" aria-hidden="true"></i> Min/views</a></th>
                    <th><a href="#" id="order" onclick="order('commentsView')"><i class="fa fa-sort" aria-hidden="true"></i> Com/view</a></th>
                    <th><a href="#" id="order" onclick="order('commentsMin')"><i class="fa fa-sort" aria-hidden="true"></i> Com/min</a></th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
    </body>
</html>