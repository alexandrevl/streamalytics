$(document).ready(function() {
    var parameters = {};
    parameters.order = "publishedAt desc";
    updateTable(parameters);
});

var lastOrder = "";
var ascDesc = "desc";

function order(order) {
    var parameters = {};
    if (order == lastOrder) {
        if (ascDesc == "desc") {
            ascDesc = "asc"
        } else {
            ascDesc = "desc"
        }
    }
    lastOrder = order;
    parameters.order = order + " " + ascDesc;
    updateTable(parameters);
}

function updateTable(parameters) {
    $("#listVideos").hide();
    $("#loading").show();
    $("#tbody").empty();
    $.ajax({
        type: 'POST',
        url: "http://streamalytics.me/api/video/",
        data: JSON.stringify(parameters),
        //dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                var json = JSON.parse(resultData);
                //console.log(json);
                var i = 1;
                json.forEach(function(video) {
                    if (video.isLive == 1) {
                        $('#listVideos > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td><a href="https://www.youtube.com/watch?v=' + video.videoId + '" target="ytVideo">' + video.title + '</a></td>' +
                            '<td>' + video.gameTitle + '</td>' +
                            '<td>' + video.publishedAt + '</td>' +
                            '<td>' + parseFloat(video.duration / 60).toFixed(0) + '</td>' +
                            '<td>' + video.viewCount + '</td>' +
                            '<td>' + video.likeCount + '</td>' +
                            '<td>' + video.dislikeCount + '</td>' +
                            '<td>' + video.estimatedMinutesWatched + '</td>' +
                            '<td>' + video.comments + '</td>' +
                            '<td>' + video.averageViewDuration + '</td>' +
                            '<td>' + parseFloat(video.averageViewPercentage).toFixed(2) + '</td>' +
                            '<td>' + video.subscribersGained + '</td>' +
                            '<td>' + parseFloat(video.viewsMin).toFixed(2) + '</td>' +
                            '<td>' + parseFloat(video.likesMin).toFixed(2) + '</td>' +
                            '<td>' + parseFloat(video.minsView).toFixed(2) + '</td>' +
                            '<td>' + parseFloat(video.commentsView).toFixed(2) + '</td>' +
                            '<td>' + parseFloat(video.commentsMin).toFixed(2) + '</td>' +
                            '</tr>');
                    }
                }, this);
                $("#listVideos").show();
                $("#loading").hide();
            },
            404: function(resultData) {
                console.log("erro");
            }
        }
    }).fail(function() {
        // console.log("error");
    });
}