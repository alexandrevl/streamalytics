<?php
    session_start();
    session_destroy();
    ini_set('session.gc_maxlifetime', 3600*10);
    session_set_cookie_params(3600*10);
    session_start();
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    require_once '/home/cupul629/public_html/vendor/autoload.php';

    $videoCount = 0;
    $client = new Google_Client();
    $client->setAuthConfig('/home/cupul629/client_secret.json');
    $client->setAccessType("offline");        // offline access
    $client->setIncludeGrantedScopes(true);   // incremental auth
    //$client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);
    $client->setScopes('https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/yt-analytics.readonly', 'https://www.googleapis.com/auth/yt-analytics-monetary.readonly');    
    //$client->addScope(Google_Service_YouTube::YOUTUBE_READONLY);
    $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/callback.php'); 

    if (isset($_GET['code'])) {
        $client->authenticate($_GET['code']);
        $access_token = $client->getAccessToken();
        $_SESSION["access_token"] = $access_token;
        //print_r($_SESSION["access_token"]);
    }
    $client->setAccessToken($_SESSION["access_token"]); 

    //Get channelId
    $channelId = "";
    try {
        $service = new Google_Service_YouTube($client);
        $optParams = array('mine' => 'true');
        $results = $service->channels->listChannels("id", $optParams) ; 
        foreach ($results as $item) {
            //print_r($item);
            $channelId = $item['id'];
        }
        $_SESSION["auth"] = array(
            "channelId" => $channelId 
        );
        if (isset($_SESSION["siteRequest"])) {
            header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SESSION["siteRequest"], FILTER_SANITIZE_URL));
        } else {
            header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . '/actions.php', FILTER_SANITIZE_URL));
        }
    } catch (Google_Service_Exception $e) {
        if ($redirect) {
            header('Location: ' . filter_var('http://' . $_SERVER['HTTP_HOST'] . '/index.php', FILTER_SANITIZE_URL));
        } else { 
            echo 'Erro na credencial do google (client)<br>'; 
        }
    }
    exit();
?>